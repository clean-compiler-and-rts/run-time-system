
;	ONLY_REVERSAL = 1
;	MARK_NEXT_WORD_USING_BRANCH = 1
;	MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE = 1

	mov	rax,qword ptr heap_size_65+0
	xor	rbx,rbx 
	
	mov	qword ptr n_marked_words+0,rbx
	shl	rax,6

	mov	qword ptr heap_size_64_65+0,rax
	
	lea	rsi,(-4000)[rsp]

	mov	qword ptr end_stack+0,rsi 

	mov	r10,neg_heap_p3+0
	mov	r11,heap_size_64_65+0
	mov	r13,qword ptr end_stack+0
	mov	r14,0

 ifdef GC_HOOKS
	mov	rax,qword ptr gc_hook_before_mark+0
	test	rax,rax
	je	no_gc_hook_before_mark
	call	rax
no_gc_hook_before_mark:
 endif

	mov	rax,qword ptr caf_list+0

	test	rax,rax 
	je	_end_mark_cafs

_mark_cafs_lp:
	mov	rbp,qword ptr [rax]
	mov	rcx,qword ptr (-8)[rax]

	push	rcx
	lea	rcx,8[rax]
	lea	r12,8[rax+rbp*8]

	call	_mark_stack_nodes

	pop	rax 
	test	rax,rax 
	jne	_mark_cafs_lp

_end_mark_cafs:
	mov	rsi,qword ptr stack_top+0
	mov	rcx,qword ptr stack_p+0

	mov	r12,rsi 
	call	_mark_stack_nodes

continue_mark_after_pmark:
	mov	qword ptr n_marked_words+0,r14

	mov	rdi,qword ptr heap_vector+0
	lea	rcx,finalizer_list+0
	lea	rdx,free_finalizer_list+0

	mov	rbp,qword ptr [rcx]
determine_free_finalizers_after_mark:
	lea	r9,__Nil-8+0
	cmp	rbp,r9
	je	end_finalizers_after_mark

	lea	rax,[r10+rbp]
	mov	rbx,rax 
	and	rax,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	esi,dword ptr [r9+rax]
 else
	mov	esi,dword ptr (bit_set_table2)[rax]
 endif
	test	esi,dword ptr [rdi+rbx*4]
	je	finalizer_not_used_after_mark

	lea	rcx,8[rbp]
	mov	rbp,qword ptr 8[rbp]
	jmp	determine_free_finalizers_after_mark

finalizer_not_used_after_mark:
	mov	qword ptr [rdx],rbp 
	lea	rdx,8[rbp]

	mov	rbp,qword ptr 8[rbp]
	mov	qword ptr [rcx],rbp 
	jmp	determine_free_finalizers_after_mark

end_finalizers_after_mark:
	mov	qword ptr [rdx],rbp 

 ifdef GC_HOOKS
	mov	rax,qword ptr gc_hook_after_mark+0
	test	rax,rax
	je	no_gc_hook_after_mark
	call	rax
no_gc_hook_after_mark:
 endif

	call	add_garbage_collect_time

	mov	rax,qword ptr bit_vector_size+0

	mov	rdi,qword ptr n_allocated_words+0
	add	rdi,qword ptr n_marked_words+0
	shl	rdi,3

	mov	rsi,rax
	shl	rsi,3

	push	rdx 
	push	rax 

	mov	rax,rdi 
	mul	qword ptr heap_size_multiple+0
	shrd	rax,rdx,8
	shr	rdx,8

	mov	rbx,rax 
	test	rdx,rdx 
	
	pop	rax 
	pop	rdx 
	
	je	not_largest_heap

	mov	rbx,qword ptr heap_size_65+0
	shl	rbx,6

not_largest_heap:
	cmp	rbx,rsi 
	jbe	no_larger_heap
	
	mov	rsi,qword ptr heap_size_65+0
	shl	rsi,6
	cmp	rbx,rsi
	jbe	not_larger_than_heap
	mov	rbx,rsi 
not_larger_than_heap:
	mov	rax,rbx 
	shr	rax,3
	mov	qword ptr bit_vector_size+0,rax
no_larger_heap:

	mov	rbp,rax

	mov	rdi,qword ptr heap_vector+0

	shr	rbp,5

	test	al,31
	je	no_extra_word

	mov	dword ptr [rdi+rbp*4],0

no_extra_word:
	sub	rax,qword ptr n_marked_words+0
	shl	rax,3
	mov	qword ptr n_last_heap_free_bytes+0,rax 

	mov	rax,qword ptr n_marked_words+0
	shl	rax,3
	add	qword ptr total_gc_bytes+0,rax

	test	qword ptr flags+0,2
	je	_no_heap_use_message2

	mov	r12,rsp
	and	rsp,-16
 ifdef LINUX
	mov	r13,rsi
	mov	r14,rdi

	lea	rdi,marked_gc_string_1+0
 else
	sub	rsp,32

	lea	rcx,marked_gc_string_1
 endif
	call	ew_print_string

 ifdef LINUX
	mov	rdi,qword ptr n_marked_words+0
	shl	rdi,3
 else
	mov	rcx,qword ptr n_marked_words
	shl	rcx,3
 endif
	call	ew_print_int

 ifdef LINUX
	lea	rdi,heap_use_after_gc_string_2+0
 else
	lea	rcx,heap_use_after_gc_string_2
 endif
	call	ew_print_string

 ifdef LINUX
	mov	rsi,r13
	mov	rdi,r14
 endif
	mov	rsp,r12

_no_heap_use_message2:
	call	call_finalizers

	mov	rsi,qword ptr n_allocated_words+0
	xor	rbx,rbx 

	mov	rcx,rdi
	mov	qword ptr n_free_words_after_mark+0,rbx

_scan_bits:
	cmp	ebx,dword ptr [rcx]
	je	_zero_bits
	mov	dword ptr [rcx],ebx 
	add	rcx,4
	sub	rbp,1
	jne	_scan_bits

	jmp	_end_scan

_zero_bits:
	lea	rdx,4[rcx]
	add	rcx,4
	sub	rbp,1
	jne	_skip_zero_bits_lp1
	jmp	_end_bits

_skip_zero_bits_lp:
	test	rax,rax 
	jne	_end_zero_bits
_skip_zero_bits_lp1:
	mov	eax,dword ptr [rcx]
	add	rcx,4
	sub	rbp,1
	jne	_skip_zero_bits_lp

	test	rax,rax
	je	_end_bits
	mov	rax,rcx
	mov	dword ptr (-4)[rcx],ebx
	sub	rax,rdx
	jmp	_end_bits2	

_end_zero_bits:
	mov	rax,rcx 
	sub	rax,rdx 
	shl	rax,3
	add	qword ptr n_free_words_after_mark+0,rax
	mov	dword ptr (-4)[rcx],ebx

	cmp	rax,rsi
	jb	_scan_bits

_found_free_memory:
	mov	qword ptr bit_counter+0,rbp
	mov	qword ptr bit_vector_p+0,rcx

	lea	rbx,(-4)[rdx]
	sub	rbx,rdi 
	shl	rbx,6
	mov	rdi,qword ptr heap_p3+0
	add	rdi,rbx 

	mov	r15,rax
	lea	rbx,[rdi+rax*8]

	sub	r15,rsi
	mov	rsi,qword ptr stack_top+0

	mov	qword ptr heap_end_after_gc+0,rbx 

	jmp	restore_registers_after_gc_and_return

_end_bits:
	mov	rax,rcx 
	sub	rax,rdx 
	add	rax,4
_end_bits2:
	shl	rax,3
	add	qword ptr n_free_words_after_mark+0,rax
	cmp	rax,rsi 
	jae	_found_free_memory

_end_scan:
	mov	qword ptr bit_counter+0,rbp
	jmp	compact_gc

; %rcx : pointer to stack element
; %rdi : heap_vector
; %rax ,%rbp ,%rbx ,%rdx ,%rsi : free

 ifdef ONLY_REVERSAL
__end_mark_using_reversal:
	pop	rcx
	mov	(-8)[rcx],rbx
 endif

_mark_stack_nodes:
	cmp	rcx,r12
	je	_end_mark_nodes
_mark_stack_nodes_:
	mov	rbx,qword ptr [rcx]

	add	rcx,8
	lea	rdx,[r10+rbx]

	cmp	rdx,r11
	jnc	_mark_stack_nodes

	mov	rbp,rdx
	and	rdx,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	esi,dword ptr [r9+rdx]
 else
	mov	esi,dword ptr (bit_set_table2)[rdx]
 endif

	test	esi,dword ptr [rdi+rbp*4]
	jne	_mark_stack_nodes

	push	rcx
 ifdef ONLY_REVERSAL
	mov	esi,1
	jmp	__mark_node
 else
	push	0
	jmp	_mark_arguments
 endif
_mark_hnf_2:
 ifdef MARK_NEXT_WORD_USING_BRANCH
	cmp	rsi,20000000h
	jbe	fits_in_word_6
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_6:
	add	r14,3
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	shr	esi,30
	add	r14,3
	or	dword ptr 4[rdi+rbp*4],esi
  else
	lea	rsi,[rsi+rsi*4]
	add	r14,3
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif

_mark_record_2_c:
	mov	rbp,qword ptr 8[rbx]
	push	rbp

	cmp	rsp,r13
	jb	__mark_using_reversal

_mark_node2:
_shared_argument_part:
	mov	rbx,qword ptr [rbx]

_mark_node:
	lea	rdx,[r10+rbx]
	cmp	rdx,r11
	jnc	_mark_next_node

	mov	rbp,rdx
	and	rdx,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	esi,dword ptr [r9+rdx]
 else
	mov	esi,dword ptr (bit_set_table2)[rdx]
 endif

	test	esi,dword ptr [rdi+rbp*4]
	jne	_mark_next_node

_mark_arguments:
	mov	rax,qword ptr [rbx]
	test	rax,2
	je	_mark_lazy_node
	
	movzx	rcx,word ptr (-2)[rax]

	test	rcx,rcx
	je	_mark_hnf_0

 ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],esi
 endif
	add	rbx,8

	cmp	rcx,256
	jae	_mark_record

	sub	rcx,2
	je	_mark_hnf_2
	jb	_mark_hnf_1

_mark_hnf_3:
	mov	rdx,qword ptr 8[rbx]
 ifdef MARK_NEXT_WORD_USING_BRANCH
	cmp	rsi,20000000h
	jbe	fits_in_word_1
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_1:	
	add	r14,3
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	shr	esi,30
	add	r14,3
	or	dword ptr 4[rdi+rbp*4],esi
  else
	lea	rsi,[rsi+rsi*4]
	add	r14,3
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif
	lea	rax,[r10+rdx]
	mov	rbp,rax
	
	and	rax,31*8
	shr	rbp,8

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	esi,dword ptr [r9+rax]
 else
	mov	esi,dword ptr (bit_set_table2)[rax]
 endif

	test	esi,dword ptr [rdi+rbp*4]
	jne	_shared_argument_part

_no_shared_argument_part:
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],esi
	add	rcx,1

	add	r14,rcx
	lea	rax,[rax+rcx*8]
	lea	rdx,(-8)[rdx+rcx*8]

	cmp	rax,32*8
	jbe	fits_in_word_2
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_2:

	mov	rbp,qword ptr [rdx]
	sub	rcx,2
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],esi
	lea	rdx,[rdx+rcx*8]

	neg	rcx
	add	r14,1

	shr	esi,cl
	sub	r14,rcx

	or	dword ptr 4[rdi+rbp*4],esi

	mov	rbp,qword ptr [rdx]
	not	rcx
  else
	lea	rdx,[rdx+rcx*8]

	add	r14,1
	mov	rax,rsi
	shl	rsi,cl
	add	r14,rcx
	or	rsi,rax

	sub	rcx,1
	or	qword ptr [rdi+rbp*4],rsi

	mov	rbp,qword ptr [rdx]
  endif
 endif
	push	rbp

_push_hnf_args:
	mov	rbp,qword ptr (-8)[rdx]
	sub	rdx,8
	push	rbp
	sub	rcx,1
	jge	_push_hnf_args

	cmp	rsp,r13
	jae	_mark_node2

	jmp	__mark_using_reversal

_mark_hnf_1:
 ifdef MARK_NEXT_WORD_USING_BRANCH
	cmp	rsi,40000000h
	jbe	fits_in_word_4
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_4:
	add	r14,2
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	shr	esi,31
	add	r14,2
	or	dword ptr 4[rdi+rbp*4],esi
  else
	lea	rsi,[rsi+rsi*2]
	add	r14,2
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif
	mov	rbx,qword ptr [rbx]
	jmp	_mark_node

 ifndef MARK_NEXT_WORD_USING_BRANCH
	align	8
 endif

_mark_lazy_node_1:
	add	rbx,8
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],esi
	cmp	rsi,20000000h
	jbe	fits_in_word_3
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_3:
	add	r14,3
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],esi
	shr	esi,30
	add	r14,3
	or	dword ptr 4[rdi+rbp*4],esi
  else
	lea	rsi,[rsi+rsi*4]
	add	r14,3
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif

	cmp	rcx,1
	je	_mark_node2

_mark_selector_node_1:
	add	rcx,2
	mov	rdx,qword ptr [rbx]
	je	_mark_indirection_node

	lea	rsi,[r10+rdx]
	mov	rbp,rsi

	shr	rbp,8
	and	rsi,31*8

	add	rcx,1

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	esi,dword ptr [r9+rsi]
 else
	mov	esi,dword ptr (bit_set_table2)[rsi]
 endif
	jle	_mark_record_selector_node_1

	test	esi,dword ptr [rdi+rbp*4]
	jne	_mark_node3

	mov	rcx,qword ptr [rdx]
	test	rcx,2
	je	_mark_node3

	cmp	word ptr (-2)[rcx],2
	jbe	_small_tuple_or_record

_large_tuple_or_record:
	mov	rcx,qword ptr 16[rdx]
	mov	r9,rcx

	add	rcx,r10
	mov	rbp,rcx
	and	rcx,31*8
	shr	rbp,8
 ifdef PIC
	lea	r8,bit_set_table2+0
	mov	ecx,dword ptr [r8+rcx]
 else
	mov	ecx,dword ptr (bit_set_table2)[rcx]
 endif
	test	ecx,dword ptr [rdi+rbp*4]
	jne	_mark_node3

 ifdef PIC
	movsxd	rcx,dword ptr(-8)[rax]
	add	rax,rcx
 else
	mov	eax,(-8)[rax]
 endif
	lea	rcx,e__system__nind+0
	mov	qword ptr (-8)[rbx],rcx
 ifdef PIC
	movzx	eax,word ptr (4-8)[rax]
 else
	movzx	eax,word ptr 4[rax]
 endif
	mov	rcx,rbx

	cmp	rax,16
	jl	_mark_tuple_selector_node_1
	mov	rdx,r9
	je	_mark_tuple_selector_node_2
	mov	rbx,qword ptr (-24)[r9+rax]
	mov	qword ptr [rcx],rbx
	jmp	_mark_node

_mark_tuple_selector_node_2:
	mov	rbx,qword ptr [r9]
	mov	qword ptr [rcx],rbx
	jmp	_mark_node	

_small_tuple_or_record:
 ifdef PIC
	movsxd	rcx,dword ptr(-8)[rax]
	add	rax,rcx
 else
	mov	eax,(-8)[rax]
 endif
	lea	rcx,e__system__nind+0
	mov	qword ptr (-8)[rbx],rcx
 ifdef PIC
	movzx	eax,word ptr (4-8)[rax]
 else
	movzx	eax,word ptr 4[rax]
 endif
	mov	rcx,rbx
_mark_tuple_selector_node_1:
	mov	rbx,qword ptr [rdx+rax]
	mov	qword ptr [rcx],rbx
	jmp	_mark_node

_mark_record_selector_node_1:
	je	_mark_strict_record_selector_node_1

	test	esi,dword ptr [rdi+rbp*4]
	jne	_mark_node3

	mov	rcx,qword ptr [rdx]
	test	rcx,2
	je	_mark_node3

	cmp	word ptr (-2)[rcx],258
	jbe	_small_tuple_or_record

	mov	rcx,qword ptr 16[rdx]
	mov	r9,rcx

	add	rcx,r10
	mov	rbp,rcx
	and	rcx,31*8
	shr	rbp,8
 ifdef PIC
	lea	r8,bit_set_table2+0
	mov	ecx,dword ptr [r8+rcx]
 else
	mov	ecx,dword ptr (bit_set_table2)[rcx]
 endif
	test	ecx,dword ptr [rdi+rbp*4]
	jne	_mark_node3

 ifdef PIC
	movsxd	rcx,dword ptr(-8)[rax]
	add	rax,rcx
 else
	mov	eax,(-8)[rax]
 endif
	lea	rcx,e__system__nind+0
	mov	qword ptr (-8)[rbx],rcx
 ifdef PIC
	movzx	eax,word ptr (4-8)[rax]
 else
	movzx	eax,word ptr 4[rax]
 endif
	mov	rcx,rbx

	cmp	rax,16
	jle	_mark_record_selector_node_2
	mov	rdx,r9
	sub	rax,24
_mark_record_selector_node_2:
	mov	rbx,qword ptr [rdx+rax]
	mov	qword ptr [rcx],rbx
	jmp	_mark_node

_mark_strict_record_selector_node_1:
	test	esi,dword ptr [rdi+rbp*4]
	jne	_mark_node3

	mov	rcx,qword ptr [rdx]
	test	rcx,2
	je	_mark_node3

	cmp	word ptr (-2)[rcx],258
	jbe	_select_from_small_record

	mov	rcx,qword ptr 16[rdx]
	mov	r9,rcx

	add	rcx,r10
	mov	rbp,rcx
	and	rcx,31*8
	shr	rbp,8
 ifdef PIC
	lea	r8,bit_set_table2+0
	mov	ecx,dword ptr [r8+rcx]
 else
	mov	ecx,dword ptr (bit_set_table2)[rcx]
 endif
	test	ecx,dword ptr [rdi+rbp*4]
	jne	_mark_node3

_select_from_small_record:
 ifdef PIC
	movsxd	rbp,dword ptr (-8)[rax]
	add	rax,rbp
 else
	mov	eax,dword ptr (-8)[rax]
 endif
	sub	rbx,8

 ifdef PIC
	movzx	ebp,word ptr (4-8)[rax]
 else
	movzx	ebp,word ptr 4[rax]
 endif
	cmp	rbp,16
	jle	_mark_strict_record_selector_node_2
	mov	rbp,qword ptr (-24)[r9+rbp]
	jmp	_mark_strict_record_selector_node_3
_mark_strict_record_selector_node_2:
	mov	rbp,qword ptr [rdx+rbp]
_mark_strict_record_selector_node_3:
	mov	qword ptr 8[rbx],rbp

 ifdef PIC
	movzx	ebp,word ptr (6-8)[rax]
 else
	movzx	ebp,word ptr 6[rax]
 endif
	test	rbp,rbp
	je	_mark_strict_record_selector_node_5
	cmp	rbp,16
	jle	_mark_strict_record_selector_node_4
	mov	rdx,r9
	sub	rbp,24
_mark_strict_record_selector_node_4:
	mov	rbp,qword ptr [rdx+rbp]
	mov	qword ptr 16[rbx],rbp
_mark_strict_record_selector_node_5:

 ifdef PIC
	mov	rax,qword ptr ((-8)-8)[rax]
 else
	mov	rax,qword ptr (-8)[rax]
 endif
	mov	qword ptr [rbx],rax
	jmp	_mark_next_node

_mark_indirection_node:
_mark_node3:
	mov	rbx,rdx
	jmp	_mark_node

_mark_next_node:
	pop	rbx
	test	rbx,rbx
	jne	_mark_node

	pop	rcx
	cmp	rcx,r12
	jne	_mark_stack_nodes_

_end_mark_nodes:
	ret

_mark_lazy_node:
	movsxd	rcx,dword ptr (-4)[rax]
	test	rcx,rcx
	je	_mark_node2_bb

	cmp	rcx,1
	jle	_mark_lazy_node_1

	cmp	rcx,257
	jge	_mark_closure_with_unboxed_arguments

 ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],esi
 endif
	lea	rbx,[rbx+rcx*8]
 ifdef MARK_NEXT_WORD_USING_BRANCH
	lea	rdx,[rdx+rcx*8]
	add	r14,rcx
	sub	rcx,1
	add	r14,1

	cmp	rdx,32*8
	jb	fits_in_word_7
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_7:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	neg	rcx
	add	r14,1

	shr	esi,cl
	sub	r14,rcx

	or	dword ptr 4[rdi+rbp*4],esi

	not	rcx
  else
	mov	rax,rsi
	shl	rsi,cl
	add	r14,1

	or	rsi,rax
	add	r14,rcx

	sub	rcx,1
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif
_push_lazy_args:
	mov	rbp,qword ptr [rbx]
	sub	rbx,8
	push	rbp
	sub	rcx,1
	jg	_push_lazy_args

	cmp	rsp,r13
	jae	_mark_node2
	
	jmp	__mark_using_reversal

_mark_closure_with_unboxed_arguments:
	je	_mark_node2_bb

 ifdef MARK_NEXT_WORD_USING_BRANCH
	mov	rax,rcx
	and	rcx,255

	shr	rax,8
	add	rcx,1

	or	dword ptr [rdi+rbp*4],esi
	add	r14,rcx
	lea	rdx,[rdx+rcx*8]

	sub	rcx,rax

	cmp	rdx,32*8
	jbe	fits_in_word_7_
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_7_:
	sub	rcx,2
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	mov	rax,rcx
	and	rcx,255

	shr	rax,8
	or	dword ptr [rdi+rbp*4],esi
	add	r14,rcx

	neg	rcx
	shr	esi,cl
	not	rcx

	add	r14,1
	or	dword ptr 4[rdi+rbp*4],esi

	sub	rcx,rax
  else
	mov	rax,rsi
	shl	rsi,cl
	add	r14,1

	or	rsi,rax

	mov	rax,rcx
	and	rcx,255
	or	qword ptr [rdi+rbp*4],rsi

	shr	rax,8
	add	r14,rcx

	sub	rcx,1

	sub	rcx,rax
  endif
 endif
	jl	_mark_next_node

	lea	rbx,8[rbx+rcx*8]
	jne	_push_lazy_args

_mark_closure_with_one_boxed_argument:
	mov	rbx,qword ptr [rbx]
	jmp	_mark_node

_mark_hnf_0:
 ifdef PIC
	lea	r9,__STRING__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __STRING__+2
 endif
	jbe	_mark_string_or_array

 ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],esi
 endif

 ifdef PIC
	lea	r9,CHAR+2+0
	cmp	rax,r9
 else
	cmp	rax,offset CHAR+2
 endif
	ja	_mark_normal_hnf_0

_mark_real_int_bool_or_char:
 ifdef MARK_NEXT_WORD_USING_BRANCH
	add	r14,2
	cmp	rsi,40000000h
	jbe	_mark_next_node

	or	dword ptr 4[rdi+rbp*4],1
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	shr	esi,31
	add	r14,2
	or	dword ptr 4[rdi+rbp*4],esi
  else
	lea	rsi,[rsi+rsi*2]
	add	r14,2
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif
	jmp	_mark_next_node

 ifdef PIC
_mark_normal_hnf_0_:
	or	dword ptr [rdi+rbp*4],esi
 endif

_mark_normal_hnf_0:
  ifdef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],esi
  endif
	inc	r14
	jmp	_mark_next_node

_mark_node2_bb:
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],esi
	add	r14,3
	cmp	rsi,20000000h
	jbe	_mark_next_node

	or	dword ptr 4[rdi+rbp*4],1
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],esi
	add	r14,3
	shr	esi,30
	or	dword ptr 4[rdi+rbp*4],esi
  else
	lea	rsi,[rsi+rsi*4]
	add	r14,3
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif
	jmp	_mark_next_node

_mark_record:
	sub	rcx,258
	je	_mark_record_2
	jl	_mark_record_1

_mark_record_3:
 ifdef MARK_NEXT_WORD_USING_BRANCH
	add	r14,3
	cmp	rsi,20000000h
	jbe	fits_in_word_13
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_13:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	shr	esi,30
	add	r14,3
	or	dword ptr 4[rdi+rbp*4],esi
  else
	lea	rsi,[rsi+rsi*4]
	add	r14,3
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif
	mov	rdx,qword ptr 8[rbx]

	movzx	rbp,word ptr (-2+2)[rax]
	lea	rsi,[r10+rdx]

	mov	rax,rsi 
	and	rsi,31*8

	shr	rax,8
	sub	rbp,1

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rsi]
 else
	mov	edx,dword ptr (bit_set_table2)[rsi]
 endif
	jb	_mark_record_3_bb

	test	edx,dword ptr [rdi+rax*4]
	jne	_mark_node2

 ifdef MARK_NEXT_WORD_USING_BRANCH
	add	rcx,1
	or	dword ptr [rdi+rax*4],edx 
	add	r14,rcx
	lea	rsi,[rsi+rcx*8]

	cmp	rsi,32*8
	jbe	_push_record_arguments
	or	dword ptr 4[rdi+rax*4],1
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rax*4],edx
	add	r14,rcx
	neg	rcx

	add	r14,1
	shr	edx,cl

	or	dword ptr 4[rdi+rax*4],edx
  else
	add	r14,rcx

	mov	rsi,rdx
	shl	rdx,cl

	add	r14,1
	or	rdx,rsi

	or	qword ptr [rdi+rax*4],rdx
  endif
 endif

_push_record_arguments:
	mov	rdx,qword ptr 8[rbx]
	mov	rcx,rbp
	shl	rbp,3
	add	rdx,rbp
	sub	rcx,1
	jge	_push_hnf_args

	jmp	_mark_node2

_mark_record_3_bb:
	test	edx,dword ptr [rdi+rax*4]
	jne	_mark_next_node

 ifdef MARK_NEXT_WORD_USING_BRANCH
	add	rcx,1
	or	dword ptr [rdi+rax*4],edx 
	add	r14,rcx
	lea	rsi,[rsi+rcx*8]
	
	cmp	rsi,32*8
	jbe	_mark_next_node

	or	dword ptr 4[rdi+rax*4],1
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rax*4],edx
	add	r14,rcx
	neg	rcx

	shr	edx,cl
	add	r14,1

	or	dword ptr 4[rdi+rax*4],edx
  else
	add	r14,rcx

	mov	rsi,rdx
	shl	rdx,cl

	add	r14,1
	or	rdx,rsi

	or	qword ptr [rdi+rax*4],rdx
  endif
 endif
	jmp	_mark_next_node

_mark_record_2:
 ifdef MARK_NEXT_WORD_USING_BRANCH
	cmp	rsi,20000000h
	jbe	fits_in_word_12
	or	dword ptr 4[rdi+rbp*4],1
fits_in_word_12:
	add	r14,3
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	shr	esi,30
	add	r14,3
	or	dword ptr 4[rdi+rbp*4],esi
  else
	lea	rsi,[rsi+rsi*4]
	add	r14,3
	or	qword ptr [rdi+rbp*4],rsi
  endif
 endif

	cmp	word ptr (-2+2)[rax],1
	ja	_mark_record_2_c
	je	_mark_node2
	jmp	_mark_next_node

_mark_record_1:
	cmp	word ptr (-2+2)[rax],0
	jne	_mark_hnf_1

	jmp	_mark_real_int_bool_or_char

_mark_string_or_array:
	je	_mark_string_or_bool_array

 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rax,r9
	jb	_mark_normal_hnf_0_
 endif

_mark_array:
 ifdef PIC
	lea	r9,__ARRAY__R__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__R__+2
 endif
	ja	_mark_strict_basic_array
	jb	_mark_lazy_array

	mov	rcx,qword ptr 16[rbx]
	movzx	rax,word ptr (-2)[rcx]
	movzx	rcx,word ptr (-2+2)[rcx]
	test	rcx,rcx
	je	_mark_b_record_array

	cmp	rsp,r13
	jb	_mark_array_using_reversal

	sub	rax,256
	cmp	rax,rcx
	je	_mark_a_record_array

_mark_ab_record_array:
	or	dword ptr [rdi+rbp*4],esi
	mov	rcx,qword ptr 8[rbx]

	imul	rax,rcx
	add	rax,3

	add	r14,rax 
	lea	rax,(-8)[rbx+rax*8]

	add	rax,r10
	shr	rax,8

	cmp	rbp,rax
	jae	_end_set_ab_array_bits

	inc	rbp
	mov	rcx,1
	cmp	rbp,rax
	jae	_last_ab_array_bits

_mark_ab_array_lp:
	or	dword ptr [rdi+rbp*4],ecx
	inc	rbp
	cmp	rbp,rax
	jb	_mark_ab_array_lp

_last_ab_array_bits:
	or	dword ptr [rdi+rbp*4],ecx

_end_set_ab_array_bits:
	mov	rax,qword ptr 8[rbx]
	mov	rdx,qword ptr 16[rbx]
	movzx	rbp,word ptr (-2+2)[rdx]
	movzx	rdx,word ptr (-2)[rdx]
	shl	rbp,3
	lea	rdx,(-2048)[rdx*8]
	push	rbp
	push	rdx
	lea	rcx,24[rbx]
	push	r12
	jmp	_mark_ab_array_begin

_mark_ab_array:
	mov	rbp,qword ptr 16[rsp]
	push	rax
	push	rcx
	lea	r12,[rcx+rbp]

	call	_mark_stack_nodes

	mov	rbp,qword ptr (8+16)[rsp]
	pop	rcx
	pop	rax
	add	rcx,rbp
_mark_ab_array_begin:
	sub	rax,1
	jnc	_mark_ab_array

	pop	r12
	add	rsp,16
	jmp	_mark_next_node

_mark_a_record_array:
	or	dword ptr [rdi+rbp*4],esi
	mov	rcx,qword ptr 8[rbx]

	imul	rax,rcx
	push	rax

	add	rax,3

	add	r14,rax 
	lea	rax,(-8)[rbx+rax*8]

	add	rax,r10
	shr	rax,8
	
	cmp	rbp,rax
	jae	_end_set_a_array_bits

	inc	rbp
	mov	rcx,1
	cmp	rbp,rax
	jae	_last_a_array_bits

_mark_a_array_lp:
	or	dword ptr [rdi+rbp*4],ecx
	inc	rbp
	cmp	rbp,rax
	jb	_mark_a_array_lp

_last_a_array_bits:
	or	dword ptr [rdi+rbp*4],ecx

_end_set_a_array_bits:
	pop	rax 
	lea	rcx,24[rbx]

	push	r12
	lea	r12,24[rbx+rax*8]

	call	_mark_stack_nodes

	pop	r12
	jmp	_mark_next_node

_mark_lazy_array:
	cmp	rsp,r13
	jb	_mark_array_using_reversal

	or	dword ptr [rdi+rbp*4],esi

 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__+2
 endif
	jne	_mark_lazy_arrayp2_

	mov	rax,qword ptr 8[rbx]

_mark_lazy_array_:
	add	rax,2

	add	r14,rax 
	lea	rax,(-8)[rbx+rax*8]

	add	rax,r10
	shr	rax,8
	
	cmp	rbp,rax
	jae	_end_set_lazy_array_bits

	inc	rbp
	mov	ecx,1
	cmp	rbp,rax
	jae	_last_lazy_array_bits

_mark_lazy_array_lp:
	or	dword ptr [rdi+rbp*4],ecx
	inc	rbp
	cmp	rbp,rax
	jb	_mark_lazy_array_lp

_last_lazy_array_bits:
	or	dword ptr [rdi+rbp*4],ecx

_end_set_lazy_array_bits:
	mov	rax,qword ptr 8[rbx]
	lea	rcx,16[rbx]

	push	r12
	lea	r12,16[rbx+rax*8]

	call	_mark_stack_nodes

	pop	r12
	jmp	_mark_next_node

_mark_lazy_arrayp2_:
	mov	rax,qword ptr 8[rbx]

	cmp	rax,1
	jbe	_mark_lazy_arrayp2_01

	sub	rax,1
	bsr	rcx,rax
	xor	eax,eax
	bts	rax,rcx

_mark_lazy_arrayp2_01:
	add	rax,rax
	jmp	_mark_lazy_array_

_mark_array_using_reversal:
	push	0
	mov	rsi,1
	jmp	__mark_node

_mark_strict_basic_array:
 ifdef PIC
	lea	r9,__ARRAY__INT__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__INT__+2
 endif
	jbe	_mark_strict_int_or_real_array

 ifdef PIC
	lea	r9,__ARRAY__BOOL__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__BOOL__+2
 endif
	ja	_mark_unboxed_basic_arrayp2
	je	_mark_string_or_bool_array

_mark_strict_int32_or_real32_array:
	mov	rax,qword ptr 8[rbx]
_mark_strict_int32_or_real32_array_:
	add	rax,4+1
	shr	rax,1
	jmp	_mark_basic_array_

_mark_strict_int_or_real_array:
	mov	rax,qword ptr 8[rbx]
	add	rax,2
	jmp	_mark_basic_array_

_mark_b_record_array:
	mov	rcx,qword ptr 8[rbx]
	sub	rax,256
	imul	rax,rcx
	add	rax,3
	jmp	_mark_basic_array_

_mark_unboxed_basic_arrayp2:
 ifdef PIC
	lea	r9,__ARRAYP2__INT__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAYP2__INT__+2
 endif
	jbe	_mark_unboxed_int_or_real_arrayp2

 ifdef PIC
	lea	r9,__ARRAYP2__BOOL__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAYP2__BOOL__+2
 endif
	jae	_mark_bool_or_char_arrayp2

_mark_unboxed_int32_or_real32_arrayp2:
	mov	rax,qword ptr 8[rbx]
	cmp	rax,2
	jbe	_mark_strict_int32_or_real32_array_

	sub	rax,1
	bsr	rcx,rax
	xor	rax,rax
	bts	rax,rcx

	add	rax,2
	jmp	_mark_basic_array_

_mark_unboxed_int_or_real_arrayp2:
	mov	rax,qword ptr 8[rbx]
	cmp	rax,1
	jbe	_mark_unboxed_int_or_real_arrayp2_01

	sub	rax,1
	bsr	rcx,rax
	xor	rax,rax
	bts	rax,rcx

_mark_unboxed_int_or_real_arrayp2_01:
	add	rax,rax
	add	rax,2
	jmp	_mark_basic_array_

_mark_bool_or_char_arrayp2:
	mov	rax,qword ptr 8[rbx]
	cmp	rax,16
	jbe	_mark_string_or_bool_array_

	sub	rax,1
	bsr	rcx,rax
	xor	rax,rax
	bts	rax,rcx

	shr	rax,wl-1
	add	rax,2
	jmp	_mark_basic_array_

_mark_string_or_bool_array:
	mov	rax,qword ptr 8[rbx]
_mark_string_or_bool_array_:
	add	rax,16+7
	shr	rax,3

_mark_basic_array_:
	or	dword ptr [rdi+rbp*4],esi

	add	r14,rax 
	lea	rax,(-8)[rbx+rax*8]

	add	rax,r10
	shr	rax,8
	
	cmp	rbp,rax
	jae	_mark_next_node

	inc	rbp
	mov	rcx,1
	cmp	rbp,rax
	jae	_last_string_bits

_mark_string_lp:
	or	dword ptr [rdi+rbp*4],ecx
	inc	rbp
	cmp	rbp,rax
	jb	_mark_string_lp

_last_string_bits:
	or	dword ptr [rdi+rbp*4],ecx
	jmp	_mark_next_node

 ifndef ONLY_REVERSAL
__end_mark_using_reversal:
 endif
	pop	rdx
	test	rdx,rdx
	je	_mark_next_node
	mov	qword ptr [rdx],rbx
	jmp	_mark_next_node

__mark_using_reversal:
	push	rbx
	mov	rsi,1
	mov	rbx,qword ptr [rbx]
	jmp	__mark_node

__mark_arguments:
	mov	rax,qword ptr [rbx]
	test	al,2
	je	__mark_lazy_node

	movzx	rcx,word ptr (-2)[rax]
	test	rcx,rcx
	je	__mark_hnf_0

	add	rbx,8

	cmp	rcx,256
	jae	__mark_record

	sub	rcx,2
	je	__mark_hnf_2
	jb	__mark_hnf_1

__mark_hnf_3:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,3

	mov	rax,qword ptr 8[rbx]

 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	cmp	rdx,20000000h
	jbe	fits__in__word__1
	or	dword ptr 4[rdi+rbp*4],1
fits__in__word__1:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],edx
	shr	edx,30
	or	dword ptr 4[rdi+rbp*4],edx
  else
	lea	rdx,[rdx+rdx*4]
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif
	add	rax,r10

	mov	rbp,rax
	and	rax,31*8

	shr	rbp,8

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rax]
 else
	mov	edx,dword ptr (bit_set_table2)[rax]
 endif
	test	edx,dword ptr [rdi+rbp*4]
	jne	__shared_argument_part

__no_shared_argument_part:
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	mov	rdx,qword ptr 8[rbx]

	mov	qword ptr 8[rbx],rsi

	add	r14,rcx
	add	rbx,8
	add	r14,1

	or	qword ptr [rdx],3

	lea	rax,[rax+rcx*8]
	lea	rsi,[rdx+rcx*8]

	cmp	rax,32*8
	jb	fits__in__word__2
	or	dword ptr 4[rdi+rbp*4],1
fits__in__word__2:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	mov	rax,rcx
	neg	rcx
	or	dword ptr [rdi+rbp*4],edx

	shr	edx,cl
	add	r14,rax

	or	dword ptr 4[rdi+rbp*4],edx
	mov	rdx,qword ptr 8[rbx]

	mov	qword ptr 8[rbx],rsi

	add	rbx,8
	add	r14,1

	or	qword ptr [rdx],3

	lea	rsi,[rdx+rax*8]
  else
	mov	rax,rdx
	shl	rdx,cl
	add	r14,1

	add	r14,rcx
	or	rdx,rax

	or	qword ptr [rdi+rbp*4],rdx

	mov	rdx,qword ptr 8[rbx]

	mov	qword ptr 8[rbx],rsi

	add	rbx,8

	or	qword ptr [rdx],3

	lea	rsi,[rdx+rcx*8]
  endif
 endif
	mov	rcx,qword ptr [rsi]
	mov	qword ptr [rsi],rbx
	mov	rbx,rcx
	jmp	__mark_node

__mark_hnf_1:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,2
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	cmp	rdx,40000000h
	jbe	__shared_argument_part
	or	dword ptr 4[rdi+rbp*4],1
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],edx
	shr	edx,31
	or	dword ptr 4[rdi+rbp*4],edx
  else
	lea	rdx,[rdx+rdx*2]
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif
__shared_argument_part:
	mov	rcx,qword ptr [rbx]
	mov	qword ptr [rbx],rsi
	lea	rsi,2[rbx]
	mov	rbx,rcx
	jmp	__mark_node

__mark_no_selector_2:
	pop	rbp
__mark_no_selector_1:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,3
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	cmp	rdx,20000000h
	jbe	__shared_argument_part

	or	dword ptr 4[rdi+rbp*4],1
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],edx
	shr	edx,30
	or	dword ptr 4[rdi+rbp*4],edx
  else
	lea	rdx,[rdx+rdx*4]
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif
	jmp	__shared_argument_part

__mark_lazy_node_1:
	je	__mark_no_selector_1

__mark_selector_node_1:
	add	rcx,2
	je	__mark_indirection_node

	add	rcx,1

	push	rbp
	mov	rcx,qword ptr [rbx]
	push	rax 
	lea	rax,[r10+rcx]

	jle	__mark_record_selector_node_1

	mov	rbp,rax
	and	rax,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	test	eax,dword ptr [rdi+rbp*4]
	pop	rax 
	jne	__mark_no_selector_2

	mov	rbp,qword ptr [rcx]
	test	ebp,2
	je	__mark_no_selector_2

	cmp	word ptr (-2)[rbp],2
	jbe	__small_tuple_or_record

__large_tuple_or_record:
	mov	r8,qword ptr 16[rcx]
 ifndef PIC
	mov	r9,r8
 endif

	add	r8,r10
	mov	rbp,r8
	and	r8,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	r8d,dword ptr [r9+r8]
 else
	mov	r8d,dword ptr (bit_set_table2)[r8]
 endif
	test	r8d,dword ptr [rdi+rbp*4]

 ifdef PIC
	mov	r9,qword ptr 16[rcx]
 endif

	jne	__mark_no_selector_2

 ifdef PIC
	movsxd	rdx,dword ptr (-8)[rax]
	add	rax,rdx
 else
	mov	eax,dword ptr (-8)[rax]
 endif
	lea	rdx,e__system__nind+0
	pop	rbp

	mov	qword ptr (-8)[rbx],rdx
 ifdef PIC
	movzx	eax,word ptr (4-8)[rax]
 else
	movzx	eax,word ptr 4[rax]
 endif
	mov	r8,rbx

	cmp	rax,16
	jl	__mark_tuple_selector_node_1
	je	__mark_tuple_selector_node_2
	mov	rbx,qword ptr (-24)[r9+rax]
	mov	qword ptr [r8],rbx
	jmp	__mark_node

__mark_tuple_selector_node_2:
	mov	rbx,qword ptr [r9]
	mov	qword ptr [r8],rbx
	jmp	__mark_node

__small_tuple_or_record:
 ifdef PIC
	movsxd	rdx,dword ptr (-8)[rax]
	add	rax,rdx
 else
	mov	eax,dword ptr (-8)[rax]
 endif
	lea	rdx,e__system__nind+0
	pop	rbp

	mov	qword ptr (-8)[rbx],rdx
 ifdef PIC
	movzx	eax,word ptr (4-8)[rax]
 else
	movzx	eax,word ptr 4[rax]
 endif
	mov	r8,rbx
__mark_tuple_selector_node_1:
	mov	rbx,qword ptr [rcx+rax]
	mov	qword ptr [r8],rbx
	jmp	__mark_node

__mark_record_selector_node_1:
	je	__mark_strict_record_selector_node_1

	mov	rbp,rax
	and	rax,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	test	eax,dword ptr [rdi+rbp*4]
	pop	rax
	jne	__mark_no_selector_2

	mov	rbp,qword ptr [rcx]
	test	ebp,2
	je	__mark_no_selector_2

	cmp	word ptr (-2)[rbp],258
	jbe	__small_record

	mov	r8,qword ptr 16[rcx]
 ifndef PIC
	mov	r9,r8
 endif

	add	r8,r10
	mov	rbp,r8
	and	r8,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	r8d,dword ptr [r9+r8]
 else
	mov	r8d,dword ptr (bit_set_table2)[r8]
 endif
	test	r8d,dword ptr [rdi+rbp*4]

 ifdef PIC
	mov	r9,qword ptr 16[rcx]
 endif

	jne	__mark_no_selector_2

__small_record:
 ifdef PIC
	movsxd	rdx,dword ptr(-8)[rax]
	add	rax,rdx
 else
	mov	eax,(-8)[rax]
 endif
	lea	rdx,e__system__nind+0
	pop	rbp

	mov	qword ptr (-8)[rbx],rdx
 ifdef PIC
	movzx	eax,word ptr (4-8)[rax]
 else
	movzx	eax,word ptr 4[rax]
 endif
	mov	r8,rbx

	cmp	rax,16
	jle	__mark_record_selector_node_2
	mov	rcx,r9
	sub	rax,24
__mark_record_selector_node_2:
	mov	rbx,qword ptr [rcx+rax]
	mov	qword ptr [r8],rbx
	jmp	__mark_node

__mark_strict_record_selector_node_1:
	mov	rbp,rax
	and	rax,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	test	eax,dword ptr [rdi+rbp *4]
	pop	rax 
	jne	__mark_no_selector_2

	mov	rbp,qword ptr [rcx]
	test	ebp,2
	je	__mark_no_selector_2

	cmp	word ptr (-2)[rbp],258
	jle	__select_from_small_record

	mov	r8,qword ptr 16[rcx]
 ifndef PIC
	mov	r9,r8
 endif

	add	r8,r10
	mov	rbp,r8
	and	r8,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	r8d,dword ptr [r9+r8]
 else
	mov	r8d,dword ptr (bit_set_table2)[r8]
 endif
	test	r8d,dword ptr [rdi+rbp*4]

 ifdef PIC
	mov	r9,qword ptr 16[rcx]
 endif

	jne	__mark_no_selector_2

__select_from_small_record:
 ifdef PIC
	movsxd	rbp,dword ptr(-8)[rax]
	add	rax,rbp
 else
	mov	eax,(-8)[rax]
 endif
	sub	rbx,8

 ifdef PIC
	movzx	ebp,word ptr (4-8)[rax]
 else
	movzx	ebp,word ptr 4[rax]
 endif
	cmp	rbp,16
	jle	__mark_strict_record_selector_node_2
	mov	rbp,qword ptr (-24)[r9+rbp]
	jmp	__mark_strict_record_selector_node_3
__mark_strict_record_selector_node_2:
	mov	rbp,qword ptr [rcx+rbp]
__mark_strict_record_selector_node_3:
	mov	qword ptr 8[rbx],rbp

 ifdef PIC
	movzx	ebp,word ptr (6-8)[rax]
 else
	movzx	ebp,word ptr 6[rax]
 endif
	test	rbp,rbp
	je	__mark_strict_record_selector_node_5
	cmp	rbp,16
	jle	__mark_strict_record_selector_node_4
	mov	rcx,r9
	sub	rbp,24
__mark_strict_record_selector_node_4:
	mov	rbp,qword ptr [rcx+rbp]
	mov	qword ptr 16[rbx],rbp
__mark_strict_record_selector_node_5:
	pop	rbp

 ifdef PIC
	mov	rax,qword ptr ((-8)-8)[rax]
 else
	mov	rax,qword ptr (-8)[rax]
 endif
	mov	qword ptr [rbx],rax
	jmp	__mark_node

__mark_indirection_node:
	mov	rbx,qword ptr [rbx]
	jmp	__mark_node

__mark_hnf_2:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,3
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	cmp	rdx,20000000h
	jbe	fits__in__word__6
	or	dword ptr 4[rdi+rbp*4],1
fits__in__word__6:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],edx
	shr	edx,30
	or	dword ptr 4[rdi+rbp*4],edx
  else
	lea	rdx,[rdx+rdx*4]
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif

__mark_record_2_c:
	mov	rax,qword ptr [rbx]
	mov	rcx,qword ptr 8[rbx]
	or	rax,2
	mov	qword ptr 8[rbx],rsi
	mov	qword ptr [rbx],rax
	lea	rsi,8[rbx]
	mov	rbx,rcx

__mark_node:
	lea	rdx,[r10+rbx]
	cmp	rdx,r11
	jae	__mark_next_node

	mov	rbp,rdx
	and	rdx,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	ecx,dword ptr [r9+rdx]
 else
	mov	ecx,dword ptr (bit_set_table2)[rdx]
 endif
	test	ecx,dword ptr [rdi+rbp*4]
	je	__mark_arguments

__mark_next_node:
	test	rsi,3
	jne	__mark_parent

	mov	rcx,qword ptr (-8)[rsi]
	mov	rdx,qword ptr [rsi]
	mov	qword ptr [rsi],rbx
	mov	qword ptr (-8)[rsi],rdx
	sub	rsi,8

	mov	rbx,rcx
	and	rcx,3
	and	rbx,-4
	or	rsi,rcx
	jmp	__mark_node

__mark_parent:
	test	rsi,2
	je	__mark_end_or_in_array

	mov	rbp,rsi
	and	rsi,-4

	mov	rcx,qword ptr [rsi]
	mov	qword ptr [rsi],rbx

	test	rbp,1
	jne	__argument_part_parent

	lea	rbx,(-8)[rsi]
	mov	rsi,rcx
	jmp	__mark_next_node

__argument_part_parent:
	and	rcx,-4
	mov	rdx,rsi 
	mov	rbx,qword ptr (-8)[rcx]
	mov	rbp,qword ptr [rcx]
	mov	qword ptr (-8)[rcx],rbp
	mov	qword ptr [rcx],rdx
	lea	rsi,(2-8)[rcx]
	jmp	__mark_node

__mark_end_or_in_array:
	sub	rsi,1
	je	__end_mark_using_reversal

	mov	rcx,qword ptr [rsi]
	mov	qword ptr [rsi],rbx

	lea	rbp,(-24)[rsi]
	cmp	rbp,rcx
	jbe	__end_array

	mov	rbp,qword ptr 16[rcx]

	movzx	rax,byte ptr (-2)[rbp]
	movzx	rbp,word ptr (-2+2)[rbp]

	cmp	rbp,1
	je	__in_array_a1

	sub	rbp,rax
	neg	rax

	or	qword ptr [rsi+rax*8],1
	lea	rsi,(-8)[rsi+rbp*8]

	mov	rbx,qword ptr [rsi]
	mov	qword ptr [rsi],rcx
	jmp	__mark_node

__in_array_a1:
	shl	rax,3
	sub	rsi,rax

	mov	rbx,qword ptr [rsi]
	mov	qword ptr [rsi],rcx
	or	rsi,1
	jmp	__mark_node

__end_array:
	mov	rsi,qword ptr [rcx]
	mov	rbx,rcx

	je	__end_record_array

	lea	r9,__ARRAY__+2+0
	mov	qword ptr [rcx],r9
	jmp	__mark_next_node

__end_record_array:
	lea	r9,__ARRAY__R__+2+0
	mov	qword ptr [rcx],r9
	jmp	__mark_next_node

__mark_lazy_node:
	movsxd	rcx,dword ptr(-4)[rax]
	test	rcx,rcx
	je	__mark_node2_bb

	add	rbx,8
	cmp	rcx,1
	jle	__mark_lazy_node_1
	cmp	rcx,257
	jge	__mark_closure_with_unboxed_arguments

 ifdef MARK_NEXT_WORD_USING_BRANCH
	add	rcx,1
	mov	rax,rdx
 endif

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif

 ifdef MARK_NEXT_WORD_USING_BRANCH
	add	r14,rcx

	lea	rax,[rax+rcx*8]
	sub	rcx,2

	or	dword ptr [rdi+rbp*4],edx

	cmp	rax,32*8
	jbe	fits__in__word__7
	or	dword ptr 4[rdi+rbp*4],1
fits__in__word__7:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	neg	rcx
	add	r14,1
	or	dword ptr [rdi+rbp*4],edx

	shr	edx,cl
	sub	r14,rcx
	not	rcx

	or	dword ptr 4[rdi+rbp*4],edx
  else
	add	r14,1
	mov	rax,rdx
	shl	rdx,cl

	or	rdx,rax
	add	r14,rcx

	sub	rcx,1
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif
__mark_closure_with_unboxed_arguments__2:
	lea	rdx,[rbx+rcx*8]
	mov	rax,qword ptr [rbx]
	or	rax,2
	mov	qword ptr [rbx],rax
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rsi 
	mov	rsi,rdx
	jmp	__mark_node

__mark_closure_with_unboxed_arguments:
	je	__mark_closure_1_with_unboxed_argument

 ifdef MARK_NEXT_WORD_USING_BRANCH
  ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rdx]
  else
	mov	eax,dword ptr (bit_set_table2)[rdx]
  endif
	or	dword ptr [rdi+rbp*4],eax
	mov	rax,rcx
	and	rcx,255

	shr	rax,8
	add	rcx,1

	lea	rdx,[rdx+rcx*8]
	add	r14,rcx
	sub	rcx,rax

	cmp	rdx,32*8
	jbe	fits__in_word_7_
	or	dword ptr 4[rdi+rbp*4],1
fits__in_word_7_:
	sub	rcx,2
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	mov	rax,rcx
	and	rcx,255

	sub	rcx,1

   ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
   else
	mov	edx,dword ptr (bit_set_table2)[rdx]
   endif

	shr	rax,8

	add	r14,rcx
	not	rcx
	or	dword ptr [rdi+rbp*4],edx

	shr	edx,cl
	not	rcx

	add	r14,2
	or	dword ptr 4[rdi+rbp*4],edx

	sub	rcx,rax
  else
   ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
   else
	mov	edx,dword ptr (bit_set_table2)[rdx]
   endif
	add	r14,1
	mov	rax,rdx
	shl	rdx,cl

	or	rdx,rax

	mov	rax,rcx
	and	rcx,255
	or	qword ptr [rdi+rbp*4],rdx

	shr	rax,8
	add	r14,rcx

	sub	rcx,1

	sub	rcx,rax
  endif
 endif
	jg	__mark_closure_with_unboxed_arguments__2
	je	__shared_argument_part
	sub	rbx,8
	jmp	__mark_next_node

__mark_closure_1_with_unboxed_argument:
	sub	rbx,8
	jmp	__mark_node2_bb

__mark_hnf_0:
 ifdef PIC
	lea	r9,dINT+2+0
	cmp	rax,r9
 else
	cmp	rax,offset dINT+2
 endif
	jne	__no_int_3

	mov	rcx,qword ptr 8[rbx]
	cmp	rcx,33
	jb	____small_int

__mark_real_bool_or_small_string:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,2
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	cmp	rdx,40000000h
	jbe	__mark_next_node
	or	dword ptr 4[rdi+rbp*4],1
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],edx
	shr	edx,31
	or	dword ptr 4[rdi+rbp*4],edx
  else
	lea	rdx,[rdx+rdx*2]
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif
	jmp	__mark_next_node

____small_int:
	shl	rcx,4
 ifdef PIC
	lea	rbx,small_integers+0
	add	rbx,rcx
 else
	lea	rbx,(small_integers)[rcx]
 endif
	jmp	__mark_next_node

__no_int_3:
 ifdef PIC
	lea	r9,__STRING__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __STRING__+2
 endif
	jbe	__mark_string_or_array

 ifdef PIC
 	lea	r9,CHAR+2+0
 	cmp	rax,r9
 else
	cmp	rax,offset CHAR+2
 endif
 	jne	__no_char_3

	movzx	rcx,byte ptr 8[rbx]
	shl	rcx,4
 ifdef PIC
	lea	rbx,static_characters+0
	add	rbx,rcx
 else
	lea	rbx,(static_characters)[rcx]
 endif
	jmp	__mark_next_node

__no_char_3:
	jb	__mark_real_bool_or_small_string

 ifdef PIC
__mark_normal_hnf_0:
 endif
	lea	rbx,((-8)-2)[rax]
	jmp	__mark_next_node

__mark_node2_bb:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,3

 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	cmp	rdx,20000000h
	jbe	__mark_next_node

	or	dword ptr 4[rdi+rbp*4],1
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],edx
	shr	edx,30
	or	dword ptr 4[rdi+rbp*4],edx
  else
	lea	rdx,[rdx+rdx*4]
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif
	jmp	__mark_next_node

__mark_record:
	sub	rcx,258
	je	__mark_record_2
	jl	__mark_record_1

__mark_record_3:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,3
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	cmp	rdx,20000000h
	jbe	fits__in__word__13
	or	dword ptr 4[rdi+rbp*4],1
fits__in__word__13:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],edx
	shr	edx,30
	or	dword ptr 4[rdi+rbp*4],edx
  else
	lea	rdx,[rdx+rdx*4]
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif
	movzx	rbp,word ptr (-2+2)[rax]

	mov	rdx,qword ptr 8[rbx]
	add	rdx,r10
	mov	rax,rdx
	and	rdx,31*8
	shr	rax,8

 ifdef MARK_NEXT_WORD_USING_BRANCH
	push	rsi 

  ifdef PIC
	lea	r9,bit_set_table2+0
	mov	esi,dword ptr [r9+rdx]
  else
	mov	esi,dword ptr (bit_set_table2)[rdx]
  endif
	test	esi,dword ptr [rdi+rax*4]
	jne	__shared_record_argument_part

	add	rcx,1
	or	dword ptr [rdi+rax*4],esi

	lea	rdx,[rdx+rcx*8]
	add	r14,rcx

	pop	rsi 

	cmp	rdx,32*8
	jbe	fits__in__word__14
	or	dword ptr 4[rdi+rax*4],1
fits__in__word__14:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
   ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
   else
	mov	edx,dword ptr (bit_set_table2)[rdx]
   endif
	test	edx,dword ptr [rdi+rax*4]
	jne	__shared_record_argument_part

	or	dword ptr [rdi+rax*4],edx
	add	r14,rcx
	neg	rcx

	add	r14,1
	shr	edx,cl

	or	dword ptr 4[rdi+rax*4],edx
  else
	push	rsi

   ifdef PIC
	lea	r9,bit_set_table2+0
	mov	esi,dword ptr [r9+rdx]
   else
	mov	esi,dword ptr (bit_set_table2)[rdx]
   endif
	test	esi,dword ptr [rdi+rax*4]
	jne	__shared_record_argument_part

	add	r14,rcx
	mov	rdx,rsi
	shl	rsi,cl

	add	r14,1
	or	rsi,rdx

	or	qword ptr [rdi+rax*4],rsi

	pop	rsi
  endif
 endif
	sub	rbp,1
	mov	rdx,qword ptr 8[rbx]
	jl	__mark_record_3_bb
	je	__shared_argument_part

	mov	qword ptr 8[rbx],rsi
	add	rbx,8

	sub	rbp,1
	je	__mark_record_3_aab

	lea	rsi,[rdx+rbp*8]
	mov	rax,qword ptr [rdx]
	or	rax,3
	mov	rcx,qword ptr [rsi]
	mov	qword ptr [rdx],rax 
	mov	qword ptr [rsi],rbx
	mov	rbx,rcx
	jmp	__mark_node

__mark_record_3_bb:
	sub	rbx,8
	jmp	__mark_next_node

__mark_record_3_aab:
	mov	rcx,qword ptr [rdx]
	mov	qword ptr [rdx],rbx
	lea	rsi,3[rdx]
	mov	rbx,rcx
	jmp	__mark_node

__shared_record_argument_part:
	mov	rdx,qword ptr 8[rbx]
 ifdef MARK_NEXT_WORD_USING_BRANCH
	pop	rsi
 else
  ifdef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	pop	rsi
  endif
 endif
	test	rbp,rbp
	jne	__shared_argument_part
	sub	rbx,8
	jmp	__mark_next_node

__mark_record_2:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,3
 ifdef MARK_NEXT_WORD_USING_BRANCH
	or	dword ptr [rdi+rbp*4],edx
	cmp	rdx,20000000h
	jbe	fits__in__word_12
	or	dword ptr 4[rdi+rbp*4],1
fits__in__word_12:
 else
  ifndef MARK_WITH_UNALIGNED_64_BIT_LOAD_STORE
	or	dword ptr [rdi+rbp*4],edx
	shr	edx,30
	or	dword ptr 4[rdi+rbp*4],edx
  else
	lea	rdx,[rdx+rdx*4]
	or	qword ptr [rdi+rbp*4],rdx
  endif
 endif
	cmp	word ptr (-2+2)[rax],1
	ja	__mark_record_2_c
	je	__shared_argument_part
	sub	rbx,8
	jmp	__mark_next_node

__mark_record_1:
	cmp	word ptr (-2+2)[rax],0
	jne	__mark_hnf_1
	sub	rbx,8
	jmp	__mark_real_bool_or_small_string

__mark_string_or_array:
	je	__mark_string_or_bool_array

 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rax,r9
	jb	__mark_normal_hnf_0
 endif

__mark_array:
 ifdef PIC
	lea	r9,__ARRAY__R__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__R__+2
 endif
	ja	__mark_strict_basic_array
	jb	__mark_lazy_array

	mov	rcx,qword ptr 16[rbx]
	movzx	rax,word ptr (-2)[rcx]
	movzx	rcx,word ptr (-2+2)[rcx]
	test	rcx,rcx
	je	__mark_b_record_array

	sub	rax,256
	cmp	rax,rcx
	je	__mark_a_record_array

__mark__ab__record__array:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	or	dword ptr [rdi+rbp*4],edx

	mov	rdx,rax
	imul	rax,qword ptr 8[rbx]

	mov	r9,rcx

	mov	rcx,r10
	add	rbx,16

	add	rcx,rbx
	lea	rcx,[rcx+rax*8]

	shr	rcx,8

	cmp	rbp,rcx
	jae	__skip_mark_array_ab_bits

	inc	rbp

__mark_array_ab_bits:
	or	dword ptr [rdi+rbp*4],1
	inc	rbp
	cmp	rbp,rcx
	jbe	__mark_array_ab_bits

__skip_mark_array_ab_bits:
	add	r14,3
	add	r14,rax

	mov	rcx,r9

	test	rax,rax
	je	__mark_array_ab_length_0

	sub	rax,rdx

	lea	rbp,(-16)[rbx]

	cmp	rcx,1
	je	__mark_array_a_length_1

	add	rcx,rax
	lea	rdx,[rbx+rcx*8]

	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rbp
	mov	qword ptr [rbp],rsi
	or	qword ptr 24[rbp+rax*8],1
	mov	rsi,rdx
	jmp	__mark_node

__mark_array_ab_length_0:
	lea	rbx,(-16)[rbx]
	jmp	__mark_next_node

__mark_array_a_length_1:
	lea	rdx,8[rbx+rax*8]

	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rbp
	mov	qword ptr [rbp],rsi
	lea	rsi,1[rdx]
	jmp	__mark_node

__mark_a_record_array:
	imul	rax,qword ptr 8[rbx]
	add	rbx,16

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	mov	rcx,r10
	or	dword ptr [rdi+rbp*4],edx
	lea	rdx,[rbx+rax*8]
	add	rcx,rdx

	shr	rcx,8

	cmp	rbp,rcx
	jae	__skip_mark_a_record_array_bits

	inc	rbp

__mark_a_record_array_bits:
	or	dword ptr [rdi+rbp*4],1
	inc	rbp
	cmp	rbp,rcx
	jbe	__mark_a_record_array_bits

__skip_mark_a_record_array_bits:
	add	r14,3
	add	r14,rax

	cmp	rax,1
	jbe	__mark_a_record_array_length_0_1

	lea	rbp,(-16)[rbx]

	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rbp
	mov	qword ptr [rbp],rsi
	or	qword ptr 24[rbp],1
	mov	rsi,rdx
	jmp	__mark_node

__mark_a_record_array_length_0_1:
	lea	rbx,(-16)[rbx]
	jne	__mark_next_node

	mov	rbp,rbx

	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rbp
	mov	qword ptr [rbp],rsi
	lea	rsi,1[rdx]
	jmp	__mark_node

__mark_lazy_array:
 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__+2
 endif
	jne	__mark_lazy_arrayp2

	mov	rax,qword ptr 8[rbx]
	add	rbx,8

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	or	dword ptr [rdi+rbp*4],edx

	lea	rdx,[rbx+rax*8]
	mov	rcx,r10
	add	rcx,rdx

	add	r14,rax

__mark_lazy_array__:
	shr	rcx,8

	cmp	rbp,rcx
	jae	__skip_mark_lazy_array_bits

	inc	rbp

__mark_lazy_array_bits:
	or	dword ptr [rdi+rbp*4],1
	inc	rbp
	cmp	rbp,rcx
	jbe	__mark_lazy_array_bits

__skip_mark_lazy_array_bits:
	add	r14,2

	cmp	rax,1
	jbe	__mark_array_length_0_1

	lea	rbp,(-8)[rbx]
	
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rbp
	mov	qword ptr [rbp],rsi
	or	qword ptr 16[rbp],1
	mov	rsi,rdx
	jmp	__mark_node

__mark_array_length_0_1:
	lea	rbx,(-8)[rbx]
	jne	__mark_next_node

	mov	rbp,rbx

	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rbp
	mov	qword ptr [rbp],rsi
	lea	rsi,1[rdx]
	jmp	__mark_node

__mark_lazy_arrayp2:
	mov	rax,qword ptr 8[rbx]
	add	rbx,8

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	or	dword ptr [rdi+rbp*4],edx

	mov	rcx,rax

	cmp	rax,1
	jbe	__mark_lazy_arrayp2_01

	sub	rcx,1
	bsr	rdx,rcx
	xor	rcx,rcx
	bts	rcx,rdx

__mark_lazy_arrayp2_01:
	add	rcx,rcx

	add	r14,rcx

	lea	rcx,[rbx+rcx*8]
	lea	rdx,[rbx+rax*8]
	add	rcx,r10

	jmp	__mark_lazy_array__

__mark_b_record_array:
	mov	rcx,qword ptr 8[rbx]
	sub	rax,256
	imul	rax,rcx
	add	rax,3
	jmp	__mark_basic_array

__mark_strict_basic_array:
 ifdef PIC
	lea	r9,__ARRAY__INT__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__INT__+2
 endif
	jbe	__mark_strict_int_or_real_array

 ifdef PIC
	lea	r9,__ARRAY__BOOL__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__BOOL__+2
 endif
 	ja	__mark_unboxed_basic_arrayp2
	je	__mark_string_or_bool_array

__mark__strict__int32__or__real32__array:
	mov	rax,qword ptr 8[rbx]
__mark__strict__int32__or__real32__array_:
	add	rax,4+1
	shr	rax,1
	jmp	__mark_basic_array

__mark_strict_int_or_real_array:
	mov	rax,qword ptr 8[rbx]
	add	rax,2
	jmp	__mark_basic_array

__mark_unboxed_basic_arrayp2:
 ifdef PIC
	lea	r9,__ARRAYP2__INT__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAYP2__INT__+2
 endif
	jbe	__mark_unboxed_int_or_real_arrayp2

 ifdef PIC
	lea	r9,__ARRAYP2__BOOL__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAYP2__BOOL__+2
 endif
	jae	__mark_unboxed_bool_or_char_arrayp2

__mark_unboxed_int32_or_real32_arrayp2:
	mov	rax,qword ptr 8[rbx]
	cmp	rax,2
	jbe	__mark__strict__int32__or__real32__array_

	sub	rax,1
	bsr	rcx,rax
	xor	rax,rax
	bts	rax,rcx

	add	rax,2
	jmp	__mark_basic_array

__mark_unboxed_int_or_real_arrayp2:
	mov	rax,qword ptr 8[rbx]
	cmp	rax,1
	jbe	__mark_unboxed_int_or_real_arrayp2_01

	sub	rax,1
	bsr	rcx,rax
	xor	rax,rax
	bts	rax,rcx

__mark_unboxed_int_or_real_arrayp2_01:
	add	rax,rax
	add	rax,2
	jmp	__mark_basic_array

__mark_unboxed_bool_or_char_arrayp2:
	mov	rax,qword ptr 8[rbx]
	cmp	rax,16
	jbe	__mark_string_or_bool_array_

	sub	rax,1
	bsr	rcx,rax
	xor	rax,rax
	bts	rax,rcx

	shr	rax,wl-1
	add	rax,2
	jmp	__mark_basic_array

__mark_string_or_bool_array:
	mov	rax,qword ptr 8[rbx]
__mark_string_or_bool_array_:
	add	rax,16+7
	shr	rax,3

__mark_basic_array:
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	edx,dword ptr [r9+rdx]
 else
	mov	edx,dword ptr (bit_set_table2)[rdx]
 endif
	add	r14,rax 

	or	dword ptr [rdi+rbp*4],edx
	lea	rax,(-8)[rbx+rax*8]

	add	rax,r10
	shr	rax,8

	cmp	rbp,rax
	jae	__mark_next_node

	inc	rbp
	mov	rcx,1

	cmp	rbp,rax
	jae	__last__string__bits

__mark_string_lp:
	or	dword ptr [rdi+rbp*4],ecx
	inc	rbp
	cmp	rbp,rax
	jb	__mark_string_lp

__last__string__bits:
	or	dword ptr [rdi+rbp*4],ecx
	jmp	__mark_next_node
