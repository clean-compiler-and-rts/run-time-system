
COPY_RECORDS_WITHOUT_POINTERS_TO_END_OF_HEAP = 1

ws	EQU	8
wl	EQU	3

	push	rsi

	mov	rdi,heap_p1+0
	neg	rdi
	mov	neg_heap_p1+0,rdi

	mov	rdi,heap_p2+0

	mov	rax,heap_size_257+0
	shl	rax,7
	mov	semi_space_size+0,rax 
	lea	rsi,[rdi+rax]

	mov	qword ptr (heap2_begin_and_end+8)+0,rsi 

 ifdef GC_HOOKS
	mov	rax,qword ptr gc_hook_before_copy+0
	test	rax,rax
	je	no_gc_hook_before_copy
	call	rax
no_gc_hook_before_copy:
 endif

	mov	rax,qword ptr caf_list+0
	test	rax,rax 
	je	end_copy_cafs

copy_cafs_lp:
	push	(-ws)[rax]
	
	lea	rbp,ws[rax]
	mov	rbx,[rax]
	sub	rbx,1
	call	copy_lp2
	
	pop	rax 
	test	rax,rax 
	jne	copy_cafs_lp

end_copy_cafs:
	mov	rbx,[rsp]
	mov	rbp,stack_p+0
	sub	rbx,rbp 
	shr	rbx,wl

	sub	rbx,1
	jb	end_copy0
	call	copy_lp2
end_copy0:
	mov	rbp,heap_p2+0

	jmp	copy_lp1
;
;	Copy all referenced nodes to the other semi space
;

in_hnf1_2:
	mov	ebx,1
copy_lp2_lp1:
	call	copy_lp2
copy_lp1:
	cmp	rbp,rdi 
	jae	end_copy1

	mov	rax,[rbp]
	add	rbp,ws
	test	al,2
	je	not_in_hnf_1
in_hnf_1:
	movzx	rbx,word ptr (-2)[rax]

	test	rbx,rbx 
	je	copy_array_21

	cmp	bl,2
	je	in_hnf1_2

	cmp	ebx,256
	jae	copy_record1_3

	push	rbx 
	xor	rbx,rbx
	
	call	copy_lp2

	pop	rbx 
	add	rbp,ws

	sub	rbx,2
	jmp	copy_lp2_lp1

copy_record1_3:
	movzx	rdx,word ptr (-2+2)[rax]

	sub	rbx,255
	sub	rdx,1

	lea	rcx,[rbp+rbx*8]
	push	rcx
	push	rdx

	xor	rbx,rbx
	call	copy_lp2

	add	rbp,ws
	pop	rbx
	dec	rbx
	call	copy_lp2

	pop	rbp
	jmp	copy_lp1

not_in_hnf_1:
	movsxd	rbx,dword ptr (-4)[rax]
	cmp	rbx,257
	jge	copy_unboxed_closure_arguments
	sub	rbx,1
	jmp	copy_lp2_lp1

copy_unboxed_closure_arguments:
	mov	rax,rbx
	shr	rax,8
	movzx	rbx,bl
	sub	rbx,rax 

	sub	rbx,1
	
	push	rax 
	call	copy_lp2
	pop	rax 

	lea	rbp,[rbp+rax*8]
	jmp	copy_lp1

copy_array_21:
 ifdef PIC
	lea	r9,__ARRAYP2__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAYP2__+2
 endif
	jb	copy_array_21_a
	je	copy_arrayp2_21_a

	mov	rbx,ws[rbp]
	add	rbp,2*ws

	movzx	rax,word ptr (-2)[rbx]
	movzx	rbx,word ptr (-2+2)[rbx]
	sub	rax,256
	cmp	rbx,rax 
	je	copy_array_21_r_a

copy_array_21_ab:
	cmp	qword ptr (-2*ws)[rbp],0
	je	copy_lp1

	sub	rax,rbx 
	shl	rax,wl
	sub	rbx,1

	push	rbx 
	push	rax 
	mov	rbx,(-2*ws)[rbp]
	sub	rbx,1
	push	rbx 

copy_array_21_lp_ab:
	mov	rbx,(2*ws)[rsp]
	call	copy_lp2

	add	rbp,qword ptr ws[rsp]
	sub	qword ptr [rsp],1
	jnc	copy_array_21_lp_ab
	
	add	rsp,3*ws
	jmp	copy_lp1

copy_array_21_r_a:
	mov	rbx,(-2*ws)[rbp]
	imul	rbx,rax
	sub	rbx,1
	jc	copy_lp1
	jmp	copy_lp2_lp1

copy_array_21_a:
	mov	rbx,[rbp]
	add	rbp,ws
	sub	rbx,1
	jc	copy_lp1
	jmp	copy_lp2_lp1

copy_arrayp2_21_a:
	mov	rbx,[rbp]
	add	rbp,ws
	sub	rbx,1
	jc	copy_lp1
	je	copy_arrayp2_21_a_1

	bsr	rcx,rbx
	mov	eax,2*ws
	shl	rax,cl

	add	rax,rbp
	push	rax
	call	copy_lp2
	pop	rbp
	jmp	copy_lp1

copy_arrayp2_21_a_1:
	call	copy_lp2
	add	rbp,ws
	jmp	copy_lp1

;
;	Copy nodes to the other semi-space
;

copy_lp2:
	mov	rdx,[rbp]

; selectors:
continue_after_selector_2:
	mov	rcx,[rdx]
	test	cl,2
	je	not_in_hnf_2

in_hnf_2:
	movzx	rax,word ptr (-2)[rcx]
	test	rax,rax
	je	copy_arity_0_node2

	cmp	rax,256
	jae	copy_record_2

	sub	rax,2
	jb	copy_hnf_node2_1
	ja	copy_hnf_node2_3

copy_hnf_node2_2:
	mov	[rbp],rdi
	lea	rbp,ws[rbp]

copy_hnf_node2_2_:
	mov	[rdi],rcx
	inc	rdi
	mov	rcx,ws[rdx]

	mov	[rdx],rdi 
	mov	rax,(2*ws)[rdx]

	sub	rbx,1
	mov	(ws-1)[rdi],rcx

	mov	(2*ws-1)[rdi],rax
	lea	rdi,(3*ws-1)[rdi]

	jae	copy_lp2
	ret

copy_hnf_node2_1:
	lea	rax,(-2*ws+1)[rsi]
	sub	rsi,2*ws
	mov	[rbp],rsi
	add	rbp,ws
	mov	[rdx],rax
	mov	[rsi],rcx
	mov	rdx,ws[rdx]
	mov	ws[rsi],rdx
	jmp	copy_lp3_

copy_hnf_node2_3:
	mov	d2,(2*ws)[rdx]

	test	qword ptr [d2],1
	jne	arguments_already_copied_2

	mov	[rbp],rdi
	add	rbp,ws

copy_hnf_node2_3_:
	mov	[rdi],rcx
	mov	rcx,d2
	lea	d2,1[rdi]
	mov	[rdx],d2

	mov	rdx,ws[rdx]
	mov	ws[rdi],rdx

	add	rdi,3*ws
	mov	rdx,[rcx]

	mov	(-ws)[rdi],rdi
	add	rcx,ws

	mov	[rdi],rdx 
	inc	rdi

	mov	(-ws)[rcx],rdi
	add	rdi,ws-1

cp_hnf_arg_lp2:
	mov	rdx,[rcx]
	add	rcx,ws
	mov	[rdi],rdx 
	add	rdi,ws
	dec	rax
	jne	cp_hnf_arg_lp2

	sub	rbx,1
	jae	copy_lp2
	ret

arguments_already_copied_2:
	mov	(-3*ws)[rsi],rcx
	mov	rcx,[d2]

arguments_already_copied_2_:
	lea	rax,(-3*ws+1)[rsi]
	sub	rsi,3*ws
	mov	[rbp],rsi
	add	rbp,ws
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	sub	rcx,1
	mov	ws[rsi],rdx
	mov	(2*ws)[rsi],rcx
	jmp	copy_lp3_

copy_arity_0_node2:
 ifdef PIC
	lea	r9,__STRING__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __STRING__+2
 endif
	jbe	copy_string_or_array_2

 ifdef PIC
	lea	r9,CHAR+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset CHAR+2
 endif
	ja	copy_normal_hnf_0_2

copy_int_bool_or_char_2:
	mov	rax,ws[rdx]
	je	copy_char_2

 ifdef PIC
	lea	r9,dINT+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset dINT+2
 endif
	jne	no_small_int_or_char_2

copy_int_2:
	cmp	rax,33
	jae	no_small_int_or_char_2
	shl	rax,wl+1
	add	rbp,ws
 ifdef PIC
	lea	r9,small_integers+0
	add	rax,r9
 else
	add	rax,offset small_integers
 endif
	sub	rbx,1
	mov	(-ws)[rbp],rax
	jae	copy_lp2
	ret

copy_char_2:
	movzx	rax,al
	shl	rax,wl+1
	add	rbp,ws
 ifdef PIC
	lea	r9,static_characters+0
	add	rax,r9
 else
	add	rax,offset static_characters
 endif
	sub	rbx,1
	mov	(-ws)[rbp],rax
	jae	copy_lp2
	ret
	
no_small_int_or_char_2:

copy_record_node2_1_b:
	mov	(-2*ws)[rsi],rcx
	add	rbp,ws
	mov	(-ws)[rsi],rax
	sub	rsi,2*ws-1

	mov	[rdx],rsi 
	dec	rsi

	mov	(-ws)[rbp],rsi

	sub	rbx,1
	jae	copy_lp2
	ret

copy_normal_hnf_0_2:
	sub	rcx,2-(-8)
	sub	rbx,1

	mov	[rbp],rcx 
	lea	rbp,ws[rbp]
	jae	copy_lp2
	ret

copy_record_node2_bb:
	mov	(-3*ws)[rsi],rcx
	sub	rsi,3*ws-1

	mov	[rdx],rsi
	dec	rsi

	mov	rax,ws[rdx]
	mov	rcx,(2*ws)[rdx]

	mov	[rbp],rsi
	add	rbp,ws

	mov	ws[rsi],rax
	sub	rbx,1

	mov	(2*ws)[rsi],rcx

	jae	copy_lp2
	ret

already_copied_2:
	dec	rcx 
	sub	rbx,1

	mov	[rbp],rcx 
	lea	rbp,ws[rbp]

	jae	copy_lp2
	ret

copy_record_2:
	sub	rax,258
	ja	copy_record_node2_3
 	jb	copy_record_node2_1
 
	cmp	word ptr (-2+2)[rcx],1
	jb	copy_record_node2_bb
	ja	copy_hnf_node2_2

copy_record_node2_2ab:
	lea	rax,(-3*ws+1)[rsi]
	mov	(-3*ws)[rsi],rcx
	sub	rsi,3*ws
	mov	rcx,(2*ws)[rdx]
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	mov	[rbp],rsi
	add	rbp,ws
	mov	(2*ws)[rsi],rcx
	mov	ws[rsi],rdx
	jmp	copy_lp3_

copy_record_node2_1:
	mov	rax,ws[rdx]

	cmp	word ptr (-2+2)[rcx],0
	je	copy_record_node2_1_b

copy_record_node2_1_a:
	mov	(-2*ws)[rsi],rcx
	lea	rcx,(-2*ws+1)[rsi]
	sub	rsi,2*ws
	mov	[rdx],rcx
	mov	rdx,rax
	mov	[rbp],rsi
	add	rbp,ws
	mov	ws[rsi],rax
	jmp	copy_lp3_

copy_record_node2_3:
 	cmp	word ptr (-2+2)[rcx],1
	ja	copy_hnf_node2_3
	jb	copy_record_node2_3_b

copy_record_node2_3_ab:
	mov	d2,(2*ws)[rdx]
	sub	d2,qword ptr heap_p1+0
	mov	d3,d2
	shr	d2,wl+1
	shr	d3,wl+4
	and	d2,31
	and	d3,-4
 ifdef PIC
	lea	r9,bit_set_table+0
	mov	d2d,dword ptr [r9+d2*4]
 else
	mov	d2d,dword ptr (bit_set_table)[d2*4]
 endif
	add	d3,qword ptr heap_copied_vector+0
	test	d2d,[d3]
	jne	record_arguments_already_copied2_3_ab

	or	[d3],d2d

	sub	rsi,3*ws+ws
	shl	rax,wl
	sub	rsi,rax
	mov	[rbp],rsi
	add	rbp,ws
	
copy_record_node2_3_ab_:
	mov	[rsi],rcx
	lea	rcx,1[rsi]
	mov	[rdx],rcx

	mov	rcx,(2*ws)[rdx]
	mov	rdx,ws[rdx]
	mov	ws[rsi],rdx
	lea	d3,(3*ws)[rsi]
	mov	(2*ws)[rsi],d3

	mov	rdx,[rcx]
	mov	[d3],rdx
	add	d3,ws
	lea	rdx,(3*ws+1)[rsi]
	mov	[rcx],rdx
	add	rcx,ws

cp_record_arg_lp2_3_ab:
	mov	rdx,[rcx]
	add	rcx,ws
	mov	[d3],rdx
	add	d3,ws
	sub	rax,ws
	jne	cp_record_arg_lp2_3_ab

	jmp	copy_lp3

record_arguments_already_copied2_3_ab:
	mov	(-3*ws)[rsi],rcx 
	mov	rcx,(2*ws)[rdx]

	mov	rcx,[rcx]
	jmp	arguments_already_copied_2_

copy_record_node2_3_b:
	mov	d2,(2*ws)[rdx]
	sub	d2,qword ptr heap_p1+0
	mov	d3,d2
	shr	d2,wl+1
	shr	d3,wl+4
	and	d2,31
	and	d3,-4
 ifdef PIC
	lea	r9,bit_set_table+0
	mov	d2d,dword ptr [r9+d2*4]
 else
	mov	d2d,dword ptr (bit_set_table)[d2*4]
 endif
	add	d3,qword ptr heap_copied_vector+0
	test	d2d,[d3]
	jne	record_arguments_already_copied2_3_b

	or	[d3],d2d

	sub	rsi,3*ws+ws
	shl	rax,wl
	sub	rsi,rax
	mov	[rbp],rsi
	add	rbp,ws

copy_record_node2_3_b_:
	mov	[rsi],rcx
	lea	rcx,1[rsi]
	mov	[rdx],rcx

	mov	rcx,(2*ws)[rdx]
	mov	rdx,ws[rdx]
	mov	ws[rsi],rdx
	lea	d3,(3*ws)[rsi]
	mov	(2*ws)[rsi],d3

	mov	rdx,[rcx]
	mov	[d3],rdx
	add	d3,ws
	lea	rdx,(3*ws+1)[rsi]
	mov	[rcx],rdx
	add	rcx,ws

cp_record_arg_lp3_b:
	mov	rdx,[rcx]
	add	rcx,ws
	mov	[d3],rdx
	add	d3,ws
	sub	rax,ws
	jne	cp_record_arg_lp3_b

	sub	rbx,1
	jae	copy_lp2
	ret

record_arguments_already_copied2_3_b:
	mov	(-3*ws)[rsi],rcx
	mov	rcx,(2*ws)[rdx]

	mov	rcx,[rcx]

	lea	rax,(-3*ws+1)[rsi]
	sub	rsi,3*ws
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	sub	rcx,1
	mov	[rbp],rsi
	add	rbp,ws
	mov	ws[rsi],rdx
	mov	(2*ws)[rsi],rcx

	sub	rbx,1
	jae	copy_lp2
	ret

not_in_hnf_2:
	test	cl,1
	jne	already_copied_2

not_in_hnf_2_:
	movsxd	rax,dword ptr (-4)[rcx]
	test	rax,rax 
	jle	copy_arity_0_node2_

copy_node2_1_:
	cmp	rax,257
	jae	copy_unboxed_closure2

	movzx	rax,al
	sub	rax,2
	jl	copy_arity_1_node2
copy_node2_3:
	mov	[rbp],rdi 
	add	rbp,ws

copy_node2_3_:
	mov	[rdi],rcx 
	inc	rdi
	mov	[rdx],rdi
	mov	rcx,ws[rdx]
	add	rdx,2*ws
	mov	(ws-1)[rdi],rcx
	add	rdi,2*ws-1

cp_arg_lp2:
	mov	rcx,[rdx]
	add	rdx,ws
	mov	[rdi],rcx 
	add	rdi,ws
	sub	rax,1
	jae	cp_arg_lp2

	sub	rbx,1
	jae	copy_lp2
	ret

copy_unboxed_closure2:
	sub	rax,1
	cmp	al,(-3)[rcx]
	jb	copy_unboxed_closure2_b_
	je	copy_unboxed_closure2_ab_
	sub	rax,1
	movzx	rax,al
	jmp	copy_node2_3

copy_unboxed_closure2_b_:
	movzx	rax,al
	test	rax,rax
	je	copy_unboxed_closure2_b_1

	sub	rsi,2*ws
	shl	rax,wl
	sub	rsi,rax
	mov	[rbp],rsi
	add	rbp,ws

copy_unboxed_closure2_b__:
	mov	[rsi],rcx

	lea	rcx,1[rsi]
	mov	[rdx],rcx

	mov	d2,ws[rdx]
	add	rdx,2*ws
	lea	rcx,(2*ws)[rsi]
	mov	ws[rsi],d2

cp_closure_lp2_b:
	mov	d2,[rdx]
	add	rdx,ws
	mov	[rcx],d2
	add	rcx,ws
	sub	rax,ws
	jne	cp_closure_lp2_b

	sub	rbx,1
	jae	copy_lp2
	ret

copy_unboxed_closure2_b_1:
	lea	rax,(-3*ws+1)[rsi]
	mov	(-3*ws)[rsi],rcx
	sub	rsi,3*ws
	mov	[rdx],rax
	mov	rax,ws[rdx]
	mov	[rbp],rsi
	add	rbp,ws
	mov	ws[rsi],rax
	sub	rbx,1
	jae	copy_lp2
	ret

copy_unboxed_closure2_ab_:
	movzx	rax,al

	sub	rsi,2*ws
	shl	rax,wl
	sub	rsi,rax
	mov	[rbp],rsi
	add	rbp,ws

copy_unboxed_closure2_ab__:
	mov	[rsi],rcx
	lea	rcx,1[rsi]
	mov	[rdx],rcx

	mov	d2,ws[rdx]
	add	rdx,2*ws
	lea	rcx,(2*ws)[rsi]
	mov	ws[rsi],d2

cp_closure_lp2_ab:
	mov	d2,[rdx]
	add	rdx,ws
	mov	[rcx],d2
	add	rcx,ws
	sub	rax,ws
	jne	cp_closure_lp2_ab

	jmp	copy_lp3

copy_arity_1_node2_:
 ifdef PROFILE_GRAPH
copy_arity_2_node2_:
	mov	rax,(2*ws)[rdx]
	mov	(-ws)[rsi],rax
 endif
copy_arity_1_node2:
	lea	rax,(-3*ws+1)[rsi]
	mov	(-3*ws)[rsi],rcx
	sub	rsi,3*ws
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	mov	[rbp],rsi
	add	rbp,ws
	mov	ws[rsi],rdx
	jmp	copy_lp3_

copy_indirection_2:
	mov	rax,rdx 
	mov	rdx,ws[rdx]

	mov	rcx,[rdx]
	test	cl,2
	jne	in_hnf_2

	test	cl,1
	jne	already_copied_2

	cmp	dword ptr (-4)[rcx],-2
	jne	not_in_hnf_2_

skip_indirections_2:
	mov	rdx,ws[rdx]

	mov	rcx,[rdx]
	test	cl,3
	jne	update_indirection_list_2

	cmp	dword ptr (-4)[rcx],-2
	je	skip_indirections_2

update_indirection_list_2:
	lea	rcx,ws[rax]
	mov	rax,ws[rax]
	mov	[rcx],rdx 
	cmp	rdx,rax 
	jne	update_indirection_list_2

	jmp	continue_after_selector_2

copy_selector_2:
	cmp	rax,-2
	je	copy_indirection_2
	jl	copy_record_selector_2

	mov	rax,ws[rdx]
	mov	d2,[rax]
	test	d2b,2
	je	copy_arity_1_node2_

 ifdef PIC
	movsxd	d3,dword ptr (-8)[rcx]
 else
	mov	d3d,dword ptr (-8)[rcx]
 endif

	cmp	word ptr (-2)[d2],2
	jbe	copy_selector_2_

	mov	d2,(2*ws)[rax]

	test	byte ptr [d2],1
	jne	copy_arity_1_node2_

 ifdef PIC
	movzx	d3,word ptr (4-8)[rcx+d3]
	lea	r9,e__system__nind+0
	mov	qword ptr [rdx],r9
 else
	movzx	d3,word ptr 4[d3]
	mov	qword ptr [rdx],offset e__system__nind
 endif

	cmp	d3,2*ws
	jl	copy_selector_2_1
	je	copy_selector_2_2

	mov	rcx,qword ptr (-3*ws)[d2+d3]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_2

copy_selector_2_1:
	mov	rcx,ws[rax]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_2

copy_selector_2_2:
	mov	rcx,[d2]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_2

copy_selector_2_:
 ifdef PIC
	movzx	d3,word ptr (4-8)[rcx+d3]
	lea	r9,e__system__nind+0
	mov	qword ptr [rdx],r9
 else
	movzx	d3,word ptr 4[d3]
	mov	qword ptr [rdx],offset e__system__nind
 endif

	mov	rcx,qword ptr [rax+d3]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_2

copy_record_selector_2:
	cmp	rax,-3
 	mov	rax,ws[rdx]
	mov	d2,[rax]
	je	copy_strict_record_selector_2

 	test	d2b,2
	je	copy_arity_1_node2_

	mov	d3d,dword ptr (-8)[rcx]

	cmp	word ptr (-2)[d2],258
	jbe	copy_record_selector_2_0

	mov	d4,qword ptr (2*ws)[rax]

	cmp	word ptr (-2+2)[d2],2
	jae	copy_record_selector_2_1

	lea	d2,(-3*ws)[d4]

	sub	d4,qword ptr heap_p1+0
	mov	d5,d4 
	shr	d4,7
	shr	d5,4
	and	d4,-4
	add	d4,qword ptr heap_copied_vector+0
	mov	d4d,[d4]
	bt	d5d,d4d
	jnc	copy_record_selector_2_0

	jmp	copy_arity_1_node2_

copy_record_selector_2_1:
	lea	d2,(-3*ws)[d4]
	test	byte ptr [d4],1
	jne	copy_arity_1_node2_

copy_record_selector_2_0:
 ifdef PIC
	movzx	d3,word ptr (4-8)[rcx+d3]
	lea	r9,e__system__nind+0
	mov	qword ptr [rdx],r9
 else
	movzx	d3,word ptr 4[d3]
	mov	qword ptr [rdx],offset e__system__nind
 endif

	cmp	d3,2*ws
	jle	copy_record_selector_2_2
	mov	rax,d2
copy_record_selector_2_2:
	mov	rcx,qword ptr [rax+d3]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_2

copy_strict_record_selector_2:
	test	d2b,2
	je	copy_arity_1_node2_

 ifdef PIC
	movsxd	d3,dword ptr (-8)[rcx]
 else
	mov	d3d,dword ptr (-8)[rcx]
 endif

	cmp	word ptr (-2)[d2],258
	jbe	copy_strict_record_selector_2_0

	mov	d4,(2*ws)[rax]

	cmp	word ptr (-2+2)[d2],2
	jb	copy_strict_record_selector_2_b

	lea	d2,(-3*ws)[d4]
	test	byte ptr [d4],1
	jne	copy_arity_1_node2_

	jmp	copy_strict_record_selector_2_0

copy_strict_record_selector_2_b:
	lea	d2,(-3*ws)[d4]

	sub	d4,qword ptr heap_p1+0
	mov	d5,d4
	shr	d4,7
	shr	d5,4
	and	d4,-4
	add	d4,qword ptr heap_copied_vector+0
	mov	d4d,[d4]
	bt	d5d,d4d
	jc	copy_arity_1_node2_

copy_strict_record_selector_2_0:
 ifdef PIC
	add	d3,rcx
 endif
 ifdef PIC
	movzx	rcx,word ptr (4-8)[d3]
 else
	movzx	rcx,word ptr 4[d3]
 endif
	cmp	rcx,2*ws
	jle	copy_strict_record_selector_2_1
	mov	rcx,qword ptr [d2+rcx]
	jmp	copy_strict_record_selector_2_2
copy_strict_record_selector_2_1:
	mov	rcx,qword ptr [rax+rcx]
copy_strict_record_selector_2_2:
	mov	qword ptr ws[rdx],rcx

 ifdef PIC
	movzx	rcx,word ptr (6-8)[d3]
 else
	movzx	rcx,word ptr 6[d3]
 endif
	test	rcx,rcx
	je	copy_strict_record_selector_2_4
	cmp	rcx,2*ws
	jle	copy_strict_record_selector_2_3
	mov	rax,d2
copy_strict_record_selector_2_3:
	mov	rcx,qword ptr [rax+rcx]
	mov	(2*ws)[rdx],rcx
copy_strict_record_selector_2_4:

 ifdef PIC
	mov	rcx,qword ptr ((-8)-8)[d3]
 else
	mov	rcx,qword ptr (-8)[d3]
 endif
	mov	[rdx],rcx
	jmp	in_hnf_2

copy_arity_0_node2_:
	jl	copy_selector_2

	mov	(-3*ws)[rsi],rcx
	sub	rsi,3*ws
	mov	[rbp],rsi 
	lea	rax,1[rsi]

	add	rbp,ws
	mov	[rdx],rax 

	sub	rbx,1
	jae	copy_lp2
	ret

copy_string_or_array_2:
 ifdef PIC
	je	copy_string_2
	lea	r9,__ARRAY__+2+0
	cmp	rcx,r9
	jb	copy_normal_hnf_0_2
	jmp	copy_array_2
copy_string_2:
 else
	jne	copy_array_2
 endif

	mov	rax,neg_heap_p1+0
	add	rax,rdx
	cmp	rax,semi_space_size+0
	jae	copy_string_or_array_constant

	mov	rcx,rdx
	mov	rdx,ws[rcx]
	add	rbp,ws

	add	rdx,ws-1
	mov	rax,rdx 
	and	rdx,-ws
	shr	rax,wl
	sub	rsi,rdx 
	sub	rsi,2*ws
	mov	(-ws)[rbp],rsi

copy_string_2_:
	mov	d2,[rcx]
	add	rcx,ws
	mov	[rsi],d2
	lea	rdx,1[rsi]
	mov	(-ws)[rcx],rdx
	lea	rdx,ws[rsi]

cp_s_arg_lp2:
	mov	d2,[rcx]
	add	rcx,ws
	mov	[rdx],d2
	add	rdx,ws
	sub	rax,1
	jge	cp_s_arg_lp2

	sub	rbx,1
	jae	copy_lp2
	ret

copy_array_2:
	mov	rax,neg_heap_p1+0
	add	rax,rdx
	cmp	rax,semi_space_size+0
	jae	copy_string_or_array_constant

 ifdef PIC
	lea	r9,__ARRAY__R__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAY__R__+2
 endif
	ja	copy_strict_basic_array_2
	jb	copy_array_a2

	mov	rax,(2*ws)[rdx]
	movzx 	d2,word ptr (-2)[rax]
	sub	d2,256
	imul	d2,qword ptr ws[rdx]

	cmp	word ptr (-2+2)[rax],0
	je	copy_array_rb2

	mov	[rdi],rcx
	mov	rcx,rdx

	mov	rdx,rdi
	lea	rdi,(3*ws)[rdi+d2*8]

	lea	rax,1[d2]
	jmp	copy_array_2_

copy_array_rb2:
	sub	rsi,3*ws
	lea	rax,1[d2]

	shl	d2,wl
	sub	rsi,d2

	mov	[rsi],rcx
	mov	rcx,rdx
	mov	rdx,rsi
	jmp	copy_array_2_

copy_array_a2:
	mov	rax,ws[rdx]

	mov	[rdi],rcx

 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAY__+2
 endif
	jne	copy_arrayp2_a2

	mov	rcx,rdx
	mov	rdx,rdi
	lea	rdi,(2*ws)[rdi+rax*8]
	jmp	copy_array_2_

copy_arrayp2_a2:
	cmp	rax,1
	jbe	copy_arrayp2_a2_01

	sub	rax,1
	bsr	rcx,rax
	mov	eax,2
	shl	rax,cl

	mov	rcx,rdx
	mov	rdx,rdi
	lea	rdi,(2*ws)[rdi+rax*8]

	mov	rax,ws[rcx]
	jmp	copy_array_2_

copy_arrayp2_a2_01:
	mov	rcx,rdx
	mov	rdx,rdi
	mov	rdi,rax
	shl	rdi,4
	lea	rdi,(2*ws)[rdi+rdx]
	jmp	copy_array_2_

copy_strict_basic_array_2:
	mov	d2,ws[rdx]

 ifdef PIC
	lea	r9,__ARRAY__INT__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAY__INT__+2
 endif
	jbe	copy_int_or_real_array_2

 ifdef PIC
	lea	r9,__ARRAY__BOOL__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAY__BOOL__+2
 endif
	ja	copy_unboxed_basic_arrayp2_2
	je	copy_bool_array_2

copy_int32_or_real32_array_2:
	add	d2,1
	shr	d2,1

copy_int_or_real_array_2:
	sub	rsi,2*ws
	mov	rax,d2
	shl	d2,wl
	sub	rsi,d2

copy_arrayp2_2:
	mov	[rsi],rcx
	mov	rcx,rdx
	mov	rdx,rsi

copy_array_2_:
	mov	[rbp],rdx
	add	rbp,ws
copy_array_2__:
	lea	d2,1[rdx]
	add	rdx,ws
	mov	[rcx],d2
	add	rcx,ws
	jmp	cp_s_arg_lp2

copy_string_or_array_constant:
	mov	[rbp],rdx
	add	rbp,ws

	sub	rbx,1
	jae	copy_lp2
	ret

copy_bool_array_2:
	add	d2,ws-1
	shr	d2,wl
	jmp	copy_int_or_real_array_2

copy_unboxed_basic_arrayp2_2:
 ifdef PIC
	lea	r9,__ARRAYP2__INT__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAYP2__INT__+2
 endif
	jbe	copy_int_or_real_arrayp2_2

 ifdef PIC
	lea	r9,__ARRAY__BOOLP2__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAYP2__BOOL__+2
 endif
	jae	copy_bool_or_char_arrayp2_2

copy_int32_or_real32_arrayp_2:
	cmp	d2,2
	jbe	copy_int32_or_real32_array_2

	lea	rax,-1[d2]
	bsr	rcx,rax
	mov	eax,2*4
	shl	rax,cl

	mov	rcx,[rdx]

	sub	rsi,2*ws
	sub	rsi,rax

	lea	rax,1[d2]
	shr	rax,1
	jmp	copy_arrayp2_2

copy_int_or_real_arrayp2_2:
	cmp	d2,1
	jbe	copy_int_or_real_arrayp2_2_01

	lea	rax,-1[d2]
	bsr	rcx,rax
	mov	eax,2*ws
	shl	rax,cl

	mov	rcx,[rdx]

	sub	rsi,2*ws
	sub	rsi,rax

	mov	rax,d2
	jmp	copy_arrayp2_2

copy_int_or_real_arrayp2_2_01:
	mov	rax,d2
	shl	d2,wl+1
	sub	rsi,2*ws
	sub	rsi,d2
	jmp	copy_arrayp2_2

copy_bool_or_char_arrayp2_2:
	cmp	d2,16
	jbe	copy_bool_array_2

	lea	rax,-1[d2]
	bsr	rcx,rax
	mov	eax,2
	shl	rax,cl

	mov	rcx,[rdx]

	sub	rsi,2*ws
	sub	rsi,rax

	lea	rax,(ws-1)[d2]
	shr	rax,wl
	jmp	copy_arrayp2_2

copy_lp3:
	mov	rdx,ws[rsi]

; selectors:
copy_lp3_:
continue_after_selector_3:
	mov	rcx,[rdx]
	test	cl,2
	je	not_in_hnf_3

in_hnf_3:
	movzx	rax,word ptr (-2)[rcx]
	test	rax,rax
	je	copy_arity_0_node3

	cmp	rax,256
	jae	copy_record_3

	sub	rax,2
	jb	copy_hnf_node3_1
	ja	copy_hnf_node3_3

	mov	ws[rsi],rdi
	jmp	copy_hnf_node2_2_

copy_hnf_node3_1:
	lea	rax,(-2*ws+1)[rsi]
	mov	(-2*ws)[rsi],rcx
	sub	rsi,2*ws
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	mov	(3*ws)[rsi],rsi
	mov	ws[rsi],rdx
	jmp	copy_lp3_

copy_hnf_node3_3:
	mov	d2,(2*ws)[rdx]

	test	qword ptr [d2],1
	jne	arguments_already_copied_3

	mov	ws[rsi],rdi
	jmp	copy_hnf_node2_3_

arguments_already_copied_3:
	mov	(-3*ws)[rsi],rcx
	mov	rcx,[d2]

arguments_already_copied_3_:
	lea	rax,(-3*ws+1)[rsi]
	sub	rsi,3*ws
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	sub	rcx,1
	mov	(4*ws)[rsi],rsi
	mov	ws[rsi],rdx
	mov	(2*ws)[rsi],rcx
	jmp	copy_lp3_

copy_arity_0_node3:
 ifdef PIC
	lea	r9,__STRING__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __STRING__+2
 endif
	jbe	copy_string_or_array_3

 ifdef PIC
	lea	r9,CHAR+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset CHAR+2
 endif
	ja	copy_normal_hnf_0_3

copy_int_bool_or_char_3:
	mov	rax,ws[rdx]
	je	copy_char_3

 ifdef PIC
	lea	r9,dINT+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset dINT+2
 endif
	jne	no_small_int_or_char_3

copy_int_3:
	cmp	rax,33
	jae	no_small_int_or_char_3
	shl	rax,4
 ifdef PIC
	lea	r9,small_integers+0
	add	rax,r9
 else
	add	rax,offset small_integers
 endif
	sub	rbx,1
	mov	ws[rsi],rax
	jae	copy_lp2
	ret

copy_char_3:
	movzx	rax,al
	shl	rax,4
 ifdef PIC
	lea	r9,static_characters+0
	add	rax,r9
 else
	add	rax,offset static_characters
 endif
	sub	rbx,1
	mov	ws[rsi],rax
	jae	copy_lp2
	ret

no_small_int_or_char_3:

copy_record_node3_1_b:
	mov	(-2*ws)[rsi],rcx
	lea	rcx,(-2*ws+1)[rsi]
	mov	(-ws)[rsi],rax
	sub	rsi,2*ws
	mov	[rdx],rcx
	mov	(3*ws)[rsi],rsi
	sub	rbx,1
	jae	copy_lp2
	ret

copy_normal_hnf_0_3:
	sub	rcx,2-(-8)
	sub	rbx,1
	mov	ws[rsi],rcx
	jae	copy_lp2
	ret

copy_record_node3_bb:
	mov	(-3*ws)[rsi],rcx
	lea	rcx,(-3*ws+1)[rsi]
	mov	[rdx],rcx
	sub	rsi,3*ws
	mov	rax,ws[rdx]
	mov	rcx,(2*ws)[rdx]
	mov	(4*ws)[rsi],rsi
	mov	ws[rsi],rax
	sub	rbx,1
	mov	(2*ws)[rsi],rcx
	jae	copy_lp2
	ret

already_copied_3:
	dec	rcx
	sub	rbx,1
	mov	ws[rsi],rcx
	jae	copy_lp2
	ret

copy_record_3:
	sub	rax,258
	ja	copy_record_node3_3
	jb	copy_record_node3_1

	cmp	word ptr (-2+2)[rcx],1
	jb	copy_record_node3_bb
	je	copy_record_node3_2ab

	mov	ws[rsi],rdi
	jmp	copy_hnf_node2_2_

copy_record_node3_2ab:
	lea	rax,(-3*ws+1)[rsi]
	mov	(-3*ws)[rsi],rcx
	sub	rsi,3*ws
	mov	rcx,(2*ws)[rdx]
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	mov	(4*ws)[rsi],rsi
	mov	(2*ws)[rsi],rcx
	mov	ws[rsi],rdx
	jmp	copy_lp3_

copy_record_node3_1:
	mov	rax,ws[rdx]

	cmp	word ptr (-2+2)[rcx],0
	je	copy_record_node3_1_b

copy_record_node3_1_a:
	mov	(-2*ws)[rsi],rcx
	lea	rcx,(-2*ws+1)[rsi]
	sub	rsi,2*ws
	mov	[rdx],rcx
	mov	rdx,rax
	mov	(3*ws)[rsi],rsi
	mov	ws[rsi],rax
	jmp	copy_lp3_

copy_record_node3_3:
	cmp	word ptr (-2+2)[rcx],1
	ja	copy_hnf_node3_3
	jb	copy_record_node3_3_b

copy_record_node3_3_ab:
	mov	d2,(2*ws)[rdx]
	sub	d2,qword ptr heap_p1+0
	mov	d3,d2
	shr	d2,4
	shr	d3,7
	and	d2,31
	and	d3,-4
 ifdef PIC
	lea	r9,bit_set_table+0
	mov	d2d,dword ptr [r9+d2*4]
 else
	mov	d2d,dword ptr (bit_set_table)[d2*4]
 endif
	add	d3,qword ptr heap_copied_vector+0
	test	d2d,[d3]
	jne	record_arguments_already_copied3_3_ab

	or	[d3],d2d

	mov	d3,rsi
	sub	rsi,3*ws+ws
	shl	rax,3
	sub	rsi,rax
	mov	ws[d3],rsi
	jmp	copy_record_node2_3_ab_

record_arguments_already_copied3_3_ab:
	mov	(-3*ws)[rsi],rcx
	mov	rcx,(2*ws)[rdx]

	mov	rcx,[rcx]
	jmp	arguments_already_copied_3_

copy_record_node3_3_b:
	mov	d2,(2*ws)[rdx]
	sub	d2,qword ptr heap_p1+0
	mov	d3,d2
	shr	d2,4
	shr	d3,7
	and	d2,31
	and	d3,-4
 ifdef PIC
	lea	r9,bit_set_table+0
	mov	d2d,dword ptr [r9+d2*4]
 else
	mov	d2d,dword ptr (bit_set_table)[d2*4]
 endif
	add	d3,qword ptr heap_copied_vector+0
	test	d2d,[d3]
	jne	record_arguments_already_copied3_3_b

	or	[d3],d2d

	mov	d3,rsi
	sub	rsi,3*ws+ws
	shl	rax,3
	sub	rsi,rax
	mov	ws[d3],rsi
	jmp	copy_record_node2_3_b_

record_arguments_already_copied3_3_b:
	mov	(-3*ws)[rsi],rcx
	mov	rcx,(2*ws)[rdx]

	mov	rcx,[rcx]

	lea	rax,(-3*ws+1)[rsi]
	sub	rsi,3*ws
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	mov	(4*ws)[rsi],rsi
	sub	rcx,1
	mov	ws[rsi],rdx
	mov	(2*ws)[rsi],rcx

	sub	rbx,1
	jae	copy_lp2
	ret

not_in_hnf_3:
	test	cl,1
	jne	already_copied_3

not_in_hnf_3_:
	movsxd	rax,dword ptr (-4)[rcx]
	test	rax,rax
	jle	copy_arity_0_node3_

	cmp	rax,257
	jae	copy_unboxed_closure3

	movzx	rax,al
	sub	rax,2
	jl	copy_arity_1_node3

copy_node3_3:
	mov	ws[rsi],rdi
	jmp	copy_node2_3_

copy_unboxed_closure3:
	sub	rax,1
	cmp	al,(-3)[rcx]
	jb	copy_unboxed_closure3_b_
	je	copy_unboxed_closure3_ab_
	sub	rax,1
	movzx	rax,al
	mov	ws[rsi],rdi
	jmp	copy_node2_3_

copy_unboxed_closure3_b_:
	movzx	rax,al
	test	rax,rax
	je	copy_unboxed_closure3_b_1

	mov	d2,rsi
	sub	rsi,2*ws
	shl	rax,3
	sub	rsi,rax
	mov	ws[d2],rsi
	jmp	copy_unboxed_closure2_b__

copy_unboxed_closure3_b_1:
	lea	rax,(-3*ws+1)[rsi]
	mov	(-3*ws)[rsi],rcx
	sub	rsi,3*ws
	mov	[rdx],rax
	mov	rax,ws[rdx]
	mov	(4*ws)[rsi],rsi
	mov	ws[rsi],rax
	sub	rbx,1
	jae	copy_lp2
	ret

copy_unboxed_closure3_ab_:
	movzx	rax,al

	mov	d2,rsi
	sub	rsi,2*ws
	shl	rax,3
	sub	rsi,rax
	mov	ws[d2],rsi
	jmp	copy_unboxed_closure2_ab__

copy_arity_1_node3_:
 ifdef PROFILE_GRAPH
	mov	rax,(2*ws)[rdx]
	mov	(-ws)[rsi],rax
 endif
copy_arity_1_node3:
	lea	rax,(-3*ws+1)[rsi]
	mov	(-3*ws)[rsi],rcx
	sub	rsi,3*ws
	mov	[rdx],rax
	mov	rdx,ws[rdx]
	mov	(4*ws)[rsi],rsi
	mov	ws[rsi],rdx
	jmp	copy_lp3_

copy_indirection_3:
	mov	rax,rdx
	mov	rdx,ws[rdx]

	mov	rcx,[rdx]
	test	cl,2
	jne	in_hnf_3

	test	cl,1
	jne	already_copied_3

	cmp	dword ptr (-4)[rcx],-2
	jne	not_in_hnf_3_

skip_indirections_3:
	mov	rdx,ws[rdx]

	mov	rcx,[rdx]
	test	cl,3
	jne	update_indirection_list_3

	cmp	dword ptr (-4)[rcx],-2
	je	skip_indirections_3

update_indirection_list_3:
	lea	rcx,ws[rax]
	mov	rax,ws[rax]
	mov	[rcx],rdx
	cmp	rdx,rax
	jne	update_indirection_list_3

	jmp	continue_after_selector_3

copy_selector_3:
	cmp	rax,-2
	je	copy_indirection_3
	jl	copy_record_selector_3

	mov	rax,ws[rdx]
	mov	d2,[rax]
	test	d2b,2
	je	copy_arity_1_node3_

 ifdef PIC
	movsxd	d3,dword ptr (-8)[rcx]
 else
	mov	d3d,dword ptr (-8)[rcx]
 endif

	cmp	word ptr (-2)[d2],2
	jbe	copy_selector_3_

	mov	d2,16[rax]
	test	byte ptr [d2],1
	jne	copy_arity_1_node3_

 ifdef PIC
	movzx	d3,word ptr (4-8)[rcx+d3]
	lea	r9,e__system__nind+0
	mov	qword ptr [rdx],r9
 else
	movzx	d3,word ptr 4[d3]
	mov	qword ptr [rdx],offset e__system__nind
 endif

	cmp	d3,2*ws
	jl	copy_selector_3_1
	je	copy_selector_3_2

	mov	rcx,qword ptr (-3*ws)[d2+d3]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_3

copy_selector_3_1:
	mov	rcx,ws[rax]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_3

copy_selector_3_2:
	mov	rcx,[d2]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_3

copy_selector_3_:
 ifdef PIC
	movzx	d3,word ptr (4-8)[rcx+d3]
	lea	r9,e__system__nind+0
	mov	qword ptr [rdx],r9
 else
	movzx	d3,word ptr 4[d3]
	mov	qword ptr [rdx],offset e__system__nind
 endif

	mov	rcx,[rax+d3]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_3

copy_record_selector_3:
	cmp	rax,-3
	mov	rax,ws[rdx]
	mov	d2,[rax]
	je	copy_strict_record_selector_3

	test	d2b,2
	je	copy_arity_1_node3_

	mov	d3d,dword ptr (-8)[rcx]

	cmp	word ptr (-2)[d2],258
	jbe	copy_record_selector_3_0

	mov	d4,qword ptr (2*ws)[rax]

	cmp	word ptr (-2+2)[d2],2
	jae	copy_record_selector_3_1

	lea	d2,(-3*ws)[d4]

	sub	d4,qword ptr heap_p1+0
	mov	d5,d4
	shr	d4,7
	shr	d5,4
	and	d4,-4
	add	d4,qword ptr heap_copied_vector+0
	mov	d4d,[d4]
	bt	d5d,d4d
	jnc	copy_record_selector_3_0

	jmp	copy_arity_1_node3_

copy_record_selector_3_1:
	lea	d2,(-3*ws)[d4]
	test	byte ptr [d4],1
	jne	copy_arity_1_node3_

copy_record_selector_3_0:
 ifdef PIC
	movzx	d3,word ptr (4-8)[rcx+d3]
	lea	r9,e__system__nind+0
	mov	qword ptr [rdx],r9
 else
	movzx	d3,word ptr 4[d3]
	mov	qword ptr [rdx],offset e__system__nind
 endif

	cmp	d3,2*ws
	jle	copy_record_selector_3_2
	mov	rax,d2
copy_record_selector_3_2:
	mov	rcx,qword ptr [rax+d3]
	mov	ws[rdx],rcx
	mov	rdx,rcx
	jmp	continue_after_selector_3

copy_strict_record_selector_3:
	test	d2b,2
	je	copy_arity_1_node3_

 ifdef PIC
	movsxd	d3,dword ptr (-8)[rcx]
 else
	mov	d3d,dword ptr (-8)[rcx]
 endif

	cmp	word ptr (-2)[d2],258
	jbe	copy_strict_record_selector_3_0

	mov	d4,qword ptr (2*ws)[rax]

	cmp	word ptr (-2+2)[d2],2
	jb	copy_strict_record_selector_3_b

	lea	d2,(-3*ws)[d4]
	test	byte ptr [d4],1
	jne	copy_arity_1_node3_

	jmp	copy_strict_record_selector_3_0

copy_strict_record_selector_3_b:
	lea	d2,(-3*ws)[d4]

	sub	d4,qword ptr heap_p1+0
	mov	d5,d4
	shr	d4,7
	shr	d5,4
	and	d4,-4
	add	d4,qword ptr heap_copied_vector+0
	mov	d4d,[d4]
	bt	d5d,d4d
	jc	copy_arity_1_node3_

copy_strict_record_selector_3_0:
 ifdef PIC
	add	d3,rcx
 endif
 ifdef PIC
	movzx	rcx,word ptr (4-8)[d3]
 else
	movzx	rcx,word ptr 4[d3]
 endif
	cmp	rcx,2*ws
	jle	copy_strict_record_selector_3_1
	mov	rcx,qword ptr [d2+rcx]
	jmp	copy_strict_record_selector_3_2
copy_strict_record_selector_3_1:
	mov	rcx,qword ptr [rax+rcx]
copy_strict_record_selector_3_2:
	mov	qword ptr ws[rdx],rcx

 ifdef PIC
	movzx	rcx,word ptr (6-8)[d3]
 else
	movzx	rcx,word ptr 6[d3]
 endif
	test	rcx,rcx
	je	copy_strict_record_selector_3_4
	cmp	rcx,2*ws
	jle	copy_strict_record_selector_3_3
	mov	rax,d2
copy_strict_record_selector_3_3:
	mov	rcx,qword ptr [rax+rcx]
	mov	(2*ws)[rdx],rcx
copy_strict_record_selector_3_4:

 ifdef PIC
	mov	rcx,qword ptr ((-8)-8)[d3]
 else
	mov	rcx,qword ptr (-8)[d3]
 endif
	mov	qword ptr [rdx],rcx
	jmp	in_hnf_3

copy_arity_0_node3_:
	jl	copy_selector_3

	mov	(-3*ws)[rsi],rcx
	lea	rax,(-3*ws+1)[rsi]
	sub	rsi,3*ws
	mov	[rdx],rax
	mov	(4*ws)[rsi],rsi

	sub	rbx,1
	jae	copy_lp2
	ret

copy_string_or_array_3:
 ifdef PIC
	je	copy_string_3
	lea	r9,__ARRAY__+2+0
	cmp	rcx,r9
	jb	copy_normal_hnf_0_3
	jmp	copy_array_3
copy_string_3:
 else
	jne	copy_array_3
 endif

	mov	rax,neg_heap_p1+0
	add	rax,rdx
	cmp	rax,semi_space_size+0
	jae	copy_string_or_array_constant_3

	mov	rcx,rdx
	mov	rdx,ws[rcx]

	mov	d2,rsi
	add	rdx,ws-1
	mov	rax,rdx 
	and	rdx,-ws
	shr	rax,3
	sub	rsi,rdx 
	sub	rsi,2*ws
	mov	ws[d2],rsi
	jmp	copy_string_2_

copy_array_3:
	mov	rax,neg_heap_p1+0
	add	rax,rdx
	cmp	rax,semi_space_size+0
	jae	copy_string_or_array_constant_3

 ifdef PIC
	lea	r9,__ARRAY__R__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAY__R__+2
 endif
	ja	copy_strict_basic_array_3
	jb	copy_array_a3

	mov	rax,(2*ws)[rdx]
	movzx 	d2,word ptr (-2)[rax]
	sub	d2,256
	imul	d2,qword ptr ws[rdx]

	cmp	word ptr (-2+2)[rax],0
	je	copy_array_rb3

	mov	[rdi],rcx
	mov	rcx,rdx
	mov	rdx,rdi
	lea	rdi,(3*ws)[rdi+d2*8]
	lea	rax,1[d2]
	mov	ws[rsi],rdx
	jmp	copy_array_2__

copy_array_rb3:
	sub	rsi,3*ws
	lea	rax,1[d2]
	shl	d2,3
	neg	d2

	mov	[rsi+d2],rcx
	mov	rcx,rdx
	add	d2,rsi

	mov	(3*ws+ws)[rsi],d2
	mov	rdx,d2
	mov	rsi,d2
	jmp	copy_array_2__

copy_array_a3:
	mov	rax,ws[rdx]

	mov	[rdi],rcx

 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAY__+2
 endif
	jne	copy_arrayp2_a3

	mov	rcx,rdx
	mov	rdx,rdi
	lea	rdi,(2*ws)[rdi+rax*8]

	mov	ws[rsi],rdx
	jmp	copy_array_2__

copy_arrayp2_a3:
	cmp	rax,1
	jbe	copy_arrayp2_a3_01

	sub	rax,1
	bsr	rcx,rax
	mov	eax,2
	shl	rax,cl

	mov	rcx,rdx
	mov	rdx,rdi
	lea	rdi,(2*ws)[rdi+rax*8]

	mov	rax,ws[rcx]

	mov	ws[rsi],rdx
	jmp	copy_array_2__

copy_arrayp2_a3_01:
	mov	rcx,rdx
	mov	rdx,rdi
	mov	rdi,rax
	shl	rdi,4
	lea	rdi,(2*ws)[rdi+rdx]

	mov	ws[rsi],rdx
	jmp	copy_array_2__

copy_strict_basic_array_3:
	mov	d2,ws[rdx]

 ifdef PIC
	lea	r9,__ARRAY__INT__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAY__INT__+2
 endif
	jbe	copy_int_or_real_array_3

 ifdef PIC
	lea	r9,__ARRAY__BOOL__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAY__BOOL__+2
 endif
	ja	copy_unboxed_basic_arrayp2_3
	je	copy_bool_array_3

copy_int32_or_real32_array_3:
	add	d2,1
	shr	d2,1

copy_int_or_real_array_3:
	sub	rsi,2*ws
	mov	rax,d2
	shl	d2,3
	neg	d2

	mov	qword ptr [rsi+d2],rcx
	mov	rcx,rdx
	add	d2,rsi

	mov	(2*ws+ws)[rsi],d2
	mov	rdx,d2
	mov	rsi,d2
	jmp	copy_array_2__

copy_string_or_array_constant_3:
	mov	ws[rsi],rdx

	sub	rbx,1
	jae	copy_lp2
	ret

copy_bool_array_3:
	add	d2,ws-1
	shr	d2,wl
	jmp	copy_int_or_real_array_3

copy_unboxed_basic_arrayp2_3:
 ifdef PIC
	lea	r9,__ARRAYP2__INT__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAYP2__INT__+2
 endif
	jbe	copy_int_or_real_arrayp2_3

 ifdef PIC
	lea	r9,__ARRAY__BOOLP2__+2+0
	cmp	rcx,r9
 else
	cmp	rcx,offset __ARRAYP2__BOOL__+2
 endif
	jae	copy_bool_or_char_arrayp2_3

copy_int32_or_real32_arrayp_3:
	cmp	d2,2
	jbe	copy_int32_or_real32_array_3

	lea	rax,(-1)[d2]
	bsr	rcx,rax
	mov	rax,-2*4
	shl	rax,cl

	mov	rcx,[rdx]

	sub	rsi,2*ws
	mov	qword ptr [rsi+rax],rcx
	add	rax,rsi
	mov	rcx,rdx

	mov	(2*ws+ws)[rsi],rax
	mov	rdx,rax
	mov	rsi,rax

	lea	rax,1[d2]
	shr	rax,1
	jmp	copy_array_2__

copy_int_or_real_arrayp2_3:
	cmp	d2,1
	jbe	copy_int_or_real_arrayp2_3_01

	lea	rax,(-1)[d2]
	bsr	rcx,rax
	mov	rax,-2*ws
	shl	rax,cl

	mov	rcx,[rdx]

copy_int_or_real_arrayp2_3_:
	sub	rsi,2*ws
	mov	qword ptr [rsi+rax],rcx
	add	rax,rsi
	mov	rcx,rdx

	mov	(2*ws+ws)[rsi],rax
	mov	rdx,rax
	mov	rsi,rax

	mov	rax,d2
	jmp	copy_array_2__

copy_int_or_real_arrayp2_3_01:
	mov	rax,d2
	shl	rax,wl+1
	neg	rax
	jmp	copy_int_or_real_arrayp2_3_

copy_bool_or_char_arrayp2_3:
	cmp	d2,16
	jbe	copy_bool_array_3

	lea	rax,(-1)[d2]
	bsr	rcx,rax
	mov	rax,-2
	shl	rax,cl

	mov	rcx,[rdx]

	sub	rsi,2*ws
	mov	qword ptr [rsi+rax],rcx
	add	rax,rsi
	mov	rcx,rdx

	mov	(2*ws+ws)[rsi],rax
	mov	rdx,rax
	mov	rsi,rax

	lea	rax,(ws-1)[d2]
	shr	rax,wl
	jmp	copy_array_2__

end_copy1:
	mov	heap_end_after_gc+0,rsi

 ifdef PIC
	lea	rcx,finalizer_list+0
 else
	mov	rcx,offset finalizer_list
 endif
 ifdef PIC
	lea	rdx,free_finalizer_list+0
 else
	mov	rdx,offset free_finalizer_list
 endif
	mov	rbp,qword ptr finalizer_list+0

determine_free_finalizers_after_copy:
	mov	rax,[rbp]
	test	al,1
	je	finalizer_not_used_after_copy

	mov	rbp,ws[rbp]
	sub	rax,1
	mov	[rcx],rax
	lea	rcx,ws[rax]
	jmp	determine_free_finalizers_after_copy

finalizer_not_used_after_copy:
	lea	r9,__Nil-8+0
	cmp	rbp,r9
	je	end_finalizers_after_copy

	mov	[rdx],rbp
	lea	rdx,ws[rbp]
	mov	rbp,ws[rbp]
	jmp	determine_free_finalizers_after_copy

end_finalizers_after_copy:
	mov	[rcx],rbp
	mov	[rdx],rbp

 ifdef GC_HOOKS
	mov	rax,qword ptr gc_hook_after_copy+0
	test	rax,rax
	je	no_gc_hook_after_copy
	call	rax
no_gc_hook_after_copy:
 endif

